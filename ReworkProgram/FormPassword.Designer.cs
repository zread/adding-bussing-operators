﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 7/6/2016
 * Time: 1:23 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ReworkProgram
{
	partial class FormPassword
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox TbPW2;
		private System.Windows.Forms.TextBox TbPW;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button btChange;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPassword));
			this.TbPW2 = new System.Windows.Forms.TextBox();
			this.TbPW = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.btChange = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// TbPW2
			// 
			this.TbPW2.Location = new System.Drawing.Point(159, 93);
			this.TbPW2.Name = "TbPW2";
			this.TbPW2.Size = new System.Drawing.Size(118, 20);
			this.TbPW2.TabIndex = 9;
			this.TbPW2.UseSystemPasswordChar = true;
			// 
			// TbPW
			// 
			this.TbPW.Location = new System.Drawing.Point(159, 60);
			this.TbPW.Name = "TbPW";
			this.TbPW.Size = new System.Drawing.Size(118, 20);
			this.TbPW.TabIndex = 8;
			this.TbPW.UseSystemPasswordChar = true;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(12, 93);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(141, 26);
			this.label2.TabIndex = 7;
			this.label2.Text = "Confirm Password";
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(70, 60);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(83, 26);
			this.label1.TabIndex = 6;
			this.label1.Text = "Password";
			// 
			// btChange
			// 
			this.btChange.Location = new System.Drawing.Point(91, 122);
			this.btChange.Name = "btChange";
			this.btChange.Size = new System.Drawing.Size(116, 28);
			this.btChange.TabIndex = 10;
			this.btChange.Text = "Change Password";
			this.btChange.UseVisualStyleBackColor = true;
			this.btChange.Click += new System.EventHandler(this.BtChangeClick);
			// 
			// FormPassword
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(312, 181);
			this.Controls.Add(this.btChange);
			this.Controls.Add(this.TbPW2);
			this.Controls.Add(this.TbPW);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormPassword";
			this.Text = "FormPassword";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
