﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 12:01 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ReworkProgram
{
	/// <summary>
	/// Description of StringClass.
	/// </summary>
	public partial class StringClass : Form
	{
		
		 public StringClass()
        {

        }
		
		private string _BoxIN = "";
		public string BoxIn
        {
            get { return _BoxIN; }
            set { _BoxIN = value; }
        }
		private string[] _SN = new string[100];
		public string[] SN
		{
			get { return _SN; }
			set { _SN = value; }
		}
		
		private string _RO = "";
		public string RO
		{
			get { return _RO; }
			set { _RO = value; }
		}
		
		private string  _ReworkComments ="";
		
			public string ReworkComments
		{
			get { return _ReworkComments; }
			set { _ReworkComments = value; }
		}
		private string  _FirstCategory ="";
		
			public string FirstCategory
		{
			get { return _FirstCategory; }
			set { _FirstCategory = value; }
		}	
		private string  _SecondCategory ="";
		
			public string SecondCategory
		{
			get { return _SecondCategory; }
			set { _SecondCategory = value; }
		}
		
		
	}
	  

	
}
