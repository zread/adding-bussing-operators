﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/22/2016
 * Time: 11:16 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormLogin.
	/// </summary>
	public partial class FormLogin : Form
	{
		
		public FormLogin()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			textBox1.Select();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		public static string usernm = "";
		public static int userID = -1;
		
		
		void LoginClick(object sender, EventArgs e)
		{
			string user,pwd;
            user = textBox1.Text.Trim();
            if (user.Length == 0)
            {
                MessageBox.Show("Username can not be empty");
                return;
            }
            
            pwd = textBox2.Text.Trim();
            if (pwd.Length == 0)
            {
                MessageBox.Show("Password can not be empty");
                return;
            }


            if (verifyUser(user, pwd))
            {
            	usernm = user.ToUpper();
				string sql =@" select top 1 UserGroup from UserLogin where UserNM ='"+ user+"' ";
				SqlDataReader rd = Toolsclass.GetDataReader(sql);
				if(rd.Read() && rd != null)
					userID = Convert.ToInt32(rd.GetValue(0));
				rd.Close();
            	MainForm frm = new MainForm();
				this.Hide();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();				
				this.Close();	
            }
            else
            {
                MessageBox.Show("Username and password is incorrect or you do not have sufficient permissions.");
            }	
		}
		
		private bool verifyUser(string username, string password)
		{
			
			//String sqlCon = @"Data Source=CA01s0015; Initial Catalog=LabelPrintDB; User ID=Fastengine; Password=Csi456";
			string sqlCon = Toolsclass.GetConnection();
			string sqlCmd = @"SELECT [UserGroup] FROM [UserLogin] WHERE [UserNM]='" + username + "' AND [UserPW]='" + password + "'";
			string result = "";
			
			using (System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(sqlCon))
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                try
                {
                    conn.Open();                  
                    cmd.CommandText = sqlCmd;
                    System.Data.SqlClient.SqlDataReader read = cmd.ExecuteReader();
                    if (read.Read())
                    {                    	
                    	result = (String.Format("{0}", read[0]));
                    		return true;
                    	                    	
                    }
					return false;                    
                }
                catch{
                	MessageBox.Show("Fail");
                	return false;
                }
			}
		}
		
		void textBox1_KeyPress(object sender, KeyPressEventArgs e )
		{
			if(e.KeyChar == '\r')
			{
				textBox2.Focus();
				textBox2.Select();
			}
		}
		void textBox2_KeyPress(object sender, KeyPressEventArgs e )
		{
			if(e.KeyChar == '\r')
			{
				Login.Focus();
				LoginClick(sender,e);
			}
			
		}
		void BtChangeClick(object sender, EventArgs e)
		{
			string user,pwd;
			
            user = textBox1.Text.Trim();
            usernm = user;
            if (user.Length == 0)
            {
                MessageBox.Show("Username can not be empty");
                return;
            }
            pwd = textBox2.Text.Trim();
            if (pwd.Length == 0)
            {
                MessageBox.Show("Password can not be empty");
                return;
            }
           FormPassword frm = new FormPassword();
				frm.StartPosition = FormStartPosition.CenterScreen;
				frm.ShowDialog();	
            
		}
		
		
	}
}
