﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/24/2016
 * Time: 2:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Linq;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormLab.
	/// </summary>
	public partial class FormLab : Form
	{
		private string[] empty = {""};
		public FormLab()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
		
			private static FormLab theSingleton = null;
		
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormLab();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		
		
		void BtSubmitClick(object sender, EventArgs e)
		{
			if(!CheckPanelData(panel1,empty))
				return;
			string  strTemp = TbSN.Text;
			  string[] sDataSet = strTemp.Split('\n');
			  int count = 0;
			  for (int i = 0; i < sDataSet.Length; i++)
                    {
                    	if(string.IsNullOrEmpty(sDataSet[i]))
                    		continue;
                    	sDataSet[i] =  sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    	
                    	string sql =@"insert into WIPModuleTracebility values('{0}','{1}','{2}','{3}','{4}',getdate())";
                    	sql = string.Format(sql,sDataSet[i],CbDT.Text,CbDep1.Text,CbDep1.Text,FormLogin.usernm);
                    	count = count + Toolsclass.PutsData(sql);
                    }
                    if (count > 0) {
			  			MessageBox.Show("Recording Succeful"); ClearAll();
                    }
                   
		}
		
		void ClearAll()
		{
			TbSN.Text = "";
			TbSN2.Text = "";
			CbDep1.SelectedIndex = -1;
			CbDep2.SelectedIndex = -1;
			CbDT.SelectedIndex = -1;
			CbDT2.SelectedIndex = -1;			
		}
		bool CheckPanelData(Panel pl, string[] exception)
		{
			foreach (Control ctl in pl.Controls) {
				if(string.IsNullOrEmpty(ctl.Text.ToString()) && !exception.Contains(ctl.Name.ToString()))
				{
					MessageBox.Show("Please Fill in the Entry of: "+ ctl.Name.ToString().Substring(2,ctl.Name.Length-2),"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
					return false;
				}
			}
			return true;
		}
		void BtSubmit2Click(object sender, EventArgs e)
		{
				if(!CheckPanelData(panel2,empty))
				return;
			string  strTemp = TbSN2.Text;
			  string[] sDataSet = strTemp.Split('\n');
			  int count = 0;
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                    	if(string.IsNullOrEmpty(sDataSet[i]))
                    		continue;
                    	sDataSet[i] =  sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    	
                    	string sql =@"insert into WIPModuleTracebility values('{0}','{1}','{2}','{3}','{4}',getdate())";
                    	sql = string.Format(sql,sDataSet[i],CbDep2.Text,CbDT2.Text,CbDT2.Text,FormLogin.usernm);
                    	count = count + Toolsclass.PutsData(sql);
                    }
                if (count > 0) {
			  			MessageBox.Show("Recording Succeful"); ClearAll();
                    }
                   
		}
		void CbDep1SelectedIndexChanged(object sender, EventArgs e)
		{
			if (CbDep1.SelectedIndex == 0) {
			
				CbDT.Items[0] = "MRB";
			}
			else
			{
				CbDT.Items[0] = "LAB";
			}
		}
		void CbDT2SelectedIndexChanged(object sender, EventArgs e)
		{
			if (CbDep2.SelectedIndex == 0) {
			
				CbDT2.Items[0] = "MRB";
			}
			else
			{
				CbDT2.Items[0] = "LAB";
			}
		}
	
	}
}
