﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 7/6/2016
 * Time: 1:23 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormPassword.
	/// </summary>
	public partial class FormPassword : Form
	{
		public FormPassword()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		
	
		void BtChangeClick(object sender, EventArgs e)
		{
			string password = TbPW.Text.ToString();
			string password2 = TbPW2.Text.ToString();
			if(string.IsNullOrEmpty(password)||string.IsNullOrEmpty(password2))
			{
				MessageBox.Show("Password cannot be empty!");
				return;
			}
			if(!password.Equals(password2))
			{
				MessageBox.Show("Password does not match!");
				return;
			}
			string sql = @"update Userlogin set [UserPW] = '{0}' where userNM = '{1}'";
			sql = string.Format(sql,password,FormLogin.usernm);
			int rev = Toolsclass.PutsData(sql);
			if(rev > 0)
			{
				MessageBox.Show("update success");			
				this.Close();
			}
			
		}	
	}
}
