﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/23/2016
 * Time: 8:40 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Linq;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormEng.
	/// </summary>
	public partial class FormEng : Form
	{
		public FormEng()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		private static FormEng _theSingleton;
		 public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FormEng
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
                         
        }
		void BtSubmitClick(object sender, EventArgs e)
		{
			string strTemp = TbSN.Text.ToString();
			string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                    	if(string.IsNullOrEmpty(sDataSet[i]))
                    		continue;
                    	sDataSet[i] =  sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    }
            
           if(!CbRO.Checked&&!CbCl.Checked&&!CbCt.Checked)
           	return;
                    
           string sql = @"update TSAP_RECEIPT_UPLOAD_MODULE set {0} where MODULE_SN in ('"+string.Join("','",sDataSet)+"')";
           string sqlro = "",sqlcl ="",sqlct="";
           if( CbRO.Checked && !string.IsNullOrEmpty(TbRO.Text) )
           {
           		sqlro = string.Format(sql,"Order_no = '"+ TbRO.Text.ToString() + "'");
           }
           
           if( CbCl.Checked && !string.IsNullOrEmpty(TbCL.Text) )
           {
           		sqlcl = string.Format(sql,"RESV04 = '"+ TbCL.Text.ToString() + "'"); 
           }
          
           if( CbCt.Checked && !string.IsNullOrEmpty(TbCt.Text) )
           {
           		sqlct = string.Format(sql,"RESV05 = '"+ TbCt.Text.ToString()+ "'"); 
           }
           string Tsql = sqlro + Environment.NewLine+sqlcl + Environment.NewLine + sqlct;
           Toolsclass.WriteToLog(Tsql,"");
           int result = Toolsclass.PutsData(Tsql);
           MessageBox.Show((result > 0)? "Successful":"Nothing Changed!");
		}
		void BtMFClick(object sender, EventArgs e)
		{
			 if(!CbRO.Checked || string.IsNullOrEmpty(TbRO.Text))                   
			 {
			 	MessageBox.Show("Please Check and Enter RO!");
			 		return;
			 }
			 	
			string strTemp = TbSN.Text.ToString();
			string[] sDataSet = strTemp.Split('\n');
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                    	if(string.IsNullOrEmpty(sDataSet[i]))
                    		continue;
                    	sDataSet[i] =  sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    }
                    string sql ="";
                    
                    
                    string[] SnNeedsAssignment = RoNeedsTobeAssigned(GetRidOfNull(sDataSet));

					if (SnNeedsAssignment.Length < 1) {
                    	ClearAll();
                    	return;
					}    
                    
                    int count = ROAssignment(SnNeedsAssignment,TbRO.Text);
                    	sql=@"update Rework_tracebility set [ModuleStatus] = {0}, ReworkOrder = '{2}' where sn in ('{1}') and [ModuleStatus] < 8 ";
//                    else
//            		sql = @"update Rework_tracebility set [ModuleStatus] = {0} where sn in ('{1}') ";
                    sql = string.Format(sql,MainForm.ED,string.Join("','",sDataSet),TbRO.Text.ToString());
             Toolsclass.WriteToLog(sql,"");
             int result = Toolsclass.PutsData(sql);
          	 MessageBox.Show((result > 0)? "Updating Rework tracebility table successful":"Nothing has been registered in Rework area yet!");
          	 MessageBox.Show((count > 0)? "Assignning Rework Order successful":"Assignning Rework Order Failed");
          	 ClearAll();
		}
		
		
		string[] GetRidOfNull(string[] sDataSet)
		{
			int count = 0;
			foreach (string SN in sDataSet) {
				if (string.IsNullOrEmpty(SN)) {
					count ++;
				}
			}
			string[] ret = new string[sDataSet.Length - count];
			count = 0;
			for (int i = 0; i < sDataSet.Length; i++) {
				if(!string.IsNullOrEmpty(sDataSet[i]))
				{
					ret[count] = sDataSet[i];
					count ++;
				}
			}
			
			return ret;
		}
		string[] RoNeedsTobeAssigned(string[] sDataSet)
		{
			string[] ret = null; // = new string[sDataSet.Length];
			string[] exist = null; // = new string[sDataSet.Length];
			string sql = @"select SN from [ReworkOrderToSerial] where sn in ('{0}') and IsFinRework = 0";
			sql = string.Format(sql,string.Join("','", sDataSet));
			DataTable dt = Toolsclass.PullData(sql);
			if (dt!= null && dt.Rows.Count > 0) {
				int count = 0;
				exist = new string[dt.Rows.Count];
				foreach (DataRow row in dt.Rows) {
					exist[count] = row[0].ToString();
					count ++;
				}
			}
			
			if(exist == null)
				return sDataSet;
			
			
				DialogResult dialogResult = MessageBox.Show("Following Serial numbers was already assigned to a rework order, Do you want to update. \n"+string.Join(" ", exist).Replace(' ','\r') ,"Warning",MessageBoxButtons.YesNo,MessageBoxIcon.Warning);
				if(dialogResult == DialogResult.Yes)
				{
					MessageBox.Show((UpdateRo(exist) > 0)?"Updating Rework Order Sucessful":"Updating Rework Order failed");
					sql=@"update Rework_tracebility set [ModuleStatus] = {0}, ReworkOrder = '{2}' where sn in ('{1}') and [ModuleStatus] < 8 ";
						sql = string.Format(sql,MainForm.ED,string.Join("','",exist),TbRO.Text.ToString());
						Toolsclass.PutsData(sql);
             		Toolsclass.WriteToLog(sql,"");					
				}
				
				ret = new string[sDataSet.Length - exist.Length];
			int countR = 0;
			foreach (string sn in sDataSet) {
				if(!exist.Contains(sn))
				{
					ret[countR] = sn;
					countR ++;
				}
			}
			return ret;
		}
		
		int UpdateRo(string[] sDataSet)
		{
			string sql = @"Update [ReworkOrderToSerial] set RO = '"+TbRO.Text+"' where SN = '{0}' and  IsFinRework = 0";
			int count = 0;
			foreach (string SN in sDataSet) {
				string sqlexe = string.Format(sql,SN);
				count += Toolsclass.PutsData(sqlexe);
			}
			return count;
		}
		int ROAssignment(string[] sDataSet, string RO)
		{
			string sql = @"insert into [ReworkOrderToSerial] values('{0}','{1}',0,0,getdate())";
			int count = 0;
			foreach (string SN in sDataSet) {
				string sqlexe = string.Format(sql,SN,RO);
				count += Toolsclass.PutsData(sqlexe);
			}
			return count;
		}
		void ClearAll()
		{
			TbSN.Clear();
			TbRO.Clear();
			TbCL.Clear();
			TbCt.Clear();
			CbRO.Checked = false;
			CbCl.Checked = false;
			CbCt.Checked = false;
		}
	}
}
