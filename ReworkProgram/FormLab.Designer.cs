﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 8/24/2016
 * Time: 2:08 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ReworkProgram
{
	partial class FormLab
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button BtSubmit;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.TextBox TbSN;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.ComboBox CbDT;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox CbDT2;
		private System.Windows.Forms.Button BtSubmit2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox TbSN2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ComboBox CbDep1;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox CbDep2;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.panel1 = new System.Windows.Forms.Panel();
			this.label6 = new System.Windows.Forms.Label();
			this.CbDep1 = new System.Windows.Forms.ComboBox();
			this.label2 = new System.Windows.Forms.Label();
			this.CbDT = new System.Windows.Forms.ComboBox();
			this.BtSubmit = new System.Windows.Forms.Button();
			this.label20 = new System.Windows.Forms.Label();
			this.TbSN = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.label7 = new System.Windows.Forms.Label();
			this.CbDep2 = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.CbDT2 = new System.Windows.Forms.ComboBox();
			this.BtSubmit2 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.TbSN2 = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label6);
			this.panel1.Controls.Add(this.CbDep1);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.CbDT);
			this.panel1.Controls.Add(this.BtSubmit);
			this.panel1.Controls.Add(this.label20);
			this.panel1.Controls.Add(this.TbSN);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(51, 74);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(430, 440);
			this.panel1.TabIndex = 7;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(240, 56);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(131, 23);
			this.label6.TabIndex = 60;
			this.label6.Text = "Receiving As:";
			// 
			// CbDep1
			// 
			this.CbDep1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDep1.FormattingEnabled = true;
			this.CbDep1.Items.AddRange(new object[] {
			"LAB",
			"MRB"});
			this.CbDep1.Location = new System.Drawing.Point(240, 82);
			this.CbDep1.Name = "CbDep1";
			this.CbDep1.Size = new System.Drawing.Size(160, 21);
			this.CbDep1.TabIndex = 59;
			this.CbDep1.SelectedIndexChanged += new System.EventHandler(this.CbDep1SelectedIndexChanged);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(240, 118);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(99, 23);
			this.label2.TabIndex = 58;
			this.label2.Text = "Module From:";
			// 
			// CbDT
			// 
			this.CbDT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDT.FormattingEnabled = true;
			this.CbDT.Items.AddRange(new object[] {
			"",
			"Production Line",
			"Warehouse",
			"Rework",
			"Other"});
			this.CbDT.Location = new System.Drawing.Point(240, 144);
			this.CbDT.Name = "CbDT";
			this.CbDT.Size = new System.Drawing.Size(160, 21);
			this.CbDT.TabIndex = 57;
			// 
			// BtSubmit
			// 
			this.BtSubmit.Location = new System.Drawing.Point(254, 394);
			this.BtSubmit.Name = "BtSubmit";
			this.BtSubmit.Size = new System.Drawing.Size(107, 25);
			this.BtSubmit.TabIndex = 55;
			this.BtSubmit.Text = "Submit";
			this.BtSubmit.UseVisualStyleBackColor = true;
			this.BtSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label20.Dock = System.Windows.Forms.DockStyle.Top;
			this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label20.ForeColor = System.Drawing.Color.Transparent;
			this.label20.Location = new System.Drawing.Point(0, 0);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(430, 35);
			this.label20.TabIndex = 48;
			this.label20.Text = "In Coming Modules";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label20.UseCompatibleTextRendering = true;
			// 
			// TbSN
			// 
			this.TbSN.Location = new System.Drawing.Point(22, 82);
			this.TbSN.Multiline = true;
			this.TbSN.Name = "TbSN";
			this.TbSN.Size = new System.Drawing.Size(195, 337);
			this.TbSN.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(22, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(99, 23);
			this.label1.TabIndex = 5;
			this.label1.Text = "SN:";
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.label7);
			this.panel2.Controls.Add(this.CbDep2);
			this.panel2.Controls.Add(this.label3);
			this.panel2.Controls.Add(this.CbDT2);
			this.panel2.Controls.Add(this.BtSubmit2);
			this.panel2.Controls.Add(this.label4);
			this.panel2.Controls.Add(this.TbSN2);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Location = new System.Drawing.Point(551, 74);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(430, 440);
			this.panel2.TabIndex = 8;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(241, 56);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(131, 23);
			this.label7.TabIndex = 62;
			this.label7.Text = "Module From:";
			// 
			// CbDep2
			// 
			this.CbDep2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDep2.FormattingEnabled = true;
			this.CbDep2.Items.AddRange(new object[] {
			"LAB",
			"MRB"});
			this.CbDep2.Location = new System.Drawing.Point(241, 82);
			this.CbDep2.Name = "CbDep2";
			this.CbDep2.Size = new System.Drawing.Size(160, 21);
			this.CbDep2.TabIndex = 61;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(241, 118);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(99, 23);
			this.label3.TabIndex = 58;
			this.label3.Text = "Delivered To:";
			// 
			// CbDT2
			// 
			this.CbDT2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDT2.FormattingEnabled = true;
			this.CbDT2.Items.AddRange(new object[] {
			"",
			"Production Line",
			"Rework",
			"Other"});
			this.CbDT2.Location = new System.Drawing.Point(241, 144);
			this.CbDT2.Name = "CbDT2";
			this.CbDT2.Size = new System.Drawing.Size(160, 21);
			this.CbDT2.TabIndex = 57;
			this.CbDT2.SelectedIndexChanged += new System.EventHandler(this.CbDT2SelectedIndexChanged);
			// 
			// BtSubmit2
			// 
			this.BtSubmit2.Location = new System.Drawing.Point(253, 394);
			this.BtSubmit2.Name = "BtSubmit2";
			this.BtSubmit2.Size = new System.Drawing.Size(107, 25);
			this.BtSubmit2.TabIndex = 55;
			this.BtSubmit2.Text = "Submit";
			this.BtSubmit2.UseVisualStyleBackColor = true;
			this.BtSubmit2.Click += new System.EventHandler(this.BtSubmit2Click);
			// 
			// label4
			// 
			this.label4.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label4.Dock = System.Windows.Forms.DockStyle.Top;
			this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label4.ForeColor = System.Drawing.Color.Transparent;
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(430, 35);
			this.label4.TabIndex = 48;
			this.label4.Text = "Out Going Modules";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label4.UseCompatibleTextRendering = true;
			// 
			// TbSN2
			// 
			this.TbSN2.Location = new System.Drawing.Point(22, 82);
			this.TbSN2.Multiline = true;
			this.TbSN2.Name = "TbSN2";
			this.TbSN2.Size = new System.Drawing.Size(195, 337);
			this.TbSN2.TabIndex = 4;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(22, 56);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(99, 23);
			this.label5.TabIndex = 5;
			this.label5.Text = "SN:";
			// 
			// FormLab
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1406, 656);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Name = "FormLab";
			this.Text = "FormLab";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
