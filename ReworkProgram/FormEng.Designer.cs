﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/23/2016
 * Time: 8:40 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ReworkProgram
{
	partial class FormEng
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.TextBox TbSN;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button BtSubmit;
		private System.Windows.Forms.TextBox TbCt;
		private System.Windows.Forms.TextBox TbCL;
		private System.Windows.Forms.TextBox TbRO;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.CheckBox CbCl;
		private System.Windows.Forms.CheckBox CbRO;
		private System.Windows.Forms.CheckBox CbCt;
		private System.Windows.Forms.Button BtMF;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.TbSN = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.BtMF = new System.Windows.Forms.Button();
			this.CbCt = new System.Windows.Forms.CheckBox();
			this.CbCl = new System.Windows.Forms.CheckBox();
			this.CbRO = new System.Windows.Forms.CheckBox();
			this.BtSubmit = new System.Windows.Forms.Button();
			this.TbCt = new System.Windows.Forms.TextBox();
			this.TbCL = new System.Windows.Forms.TextBox();
			this.TbRO = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// TbSN
			// 
			this.TbSN.Location = new System.Drawing.Point(22, 82);
			this.TbSN.Multiline = true;
			this.TbSN.Name = "TbSN";
			this.TbSN.Size = new System.Drawing.Size(195, 337);
			this.TbSN.TabIndex = 4;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(22, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(99, 23);
			this.label1.TabIndex = 5;
			this.label1.Text = "SN:";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.BtMF);
			this.panel1.Controls.Add(this.CbCt);
			this.panel1.Controls.Add(this.CbCl);
			this.panel1.Controls.Add(this.CbRO);
			this.panel1.Controls.Add(this.BtSubmit);
			this.panel1.Controls.Add(this.TbCt);
			this.panel1.Controls.Add(this.TbCL);
			this.panel1.Controls.Add(this.TbRO);
			this.panel1.Controls.Add(this.label20);
			this.panel1.Controls.Add(this.TbSN);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Location = new System.Drawing.Point(67, 77);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(698, 440);
			this.panel1.TabIndex = 6;
			// 
			// BtMF
			// 
			this.BtMF.Location = new System.Drawing.Point(316, 383);
			this.BtMF.Name = "BtMF";
			this.BtMF.Size = new System.Drawing.Size(107, 25);
			this.BtMF.TabIndex = 59;
			this.BtMF.Text = "Mark Finsh";
			this.BtMF.UseVisualStyleBackColor = true;
			this.BtMF.Click += new System.EventHandler(this.BtMFClick);
			// 
			// CbCt
			// 
			this.CbCt.Location = new System.Drawing.Point(303, 167);
			this.CbCt.Name = "CbCt";
			this.CbCt.Size = new System.Drawing.Size(110, 20);
			this.CbCt.TabIndex = 58;
			this.CbCt.Text = "Connector Type";
			this.CbCt.UseVisualStyleBackColor = true;
			// 
			// CbCl
			// 
			this.CbCl.Location = new System.Drawing.Point(303, 111);
			this.CbCl.Name = "CbCl";
			this.CbCl.Size = new System.Drawing.Size(98, 20);
			this.CbCl.TabIndex = 57;
			this.CbCl.Text = "CableLength";
			this.CbCl.UseVisualStyleBackColor = true;
			// 
			// CbRO
			// 
			this.CbRO.Location = new System.Drawing.Point(303, 63);
			this.CbRO.Name = "CbRO";
			this.CbRO.Size = new System.Drawing.Size(84, 20);
			this.CbRO.TabIndex = 56;
			this.CbRO.Text = "RO/PO";
			this.CbRO.UseVisualStyleBackColor = true;
			// 
			// BtSubmit
			// 
			this.BtSubmit.Enabled = false;
			this.BtSubmit.Location = new System.Drawing.Point(525, 383);
			this.BtSubmit.Name = "BtSubmit";
			this.BtSubmit.Size = new System.Drawing.Size(107, 25);
			this.BtSubmit.TabIndex = 55;
			this.BtSubmit.Text = "Submit";
			this.BtSubmit.UseVisualStyleBackColor = true;
			this.BtSubmit.Click += new System.EventHandler(this.BtSubmitClick);
			// 
			// TbCt
			// 
			this.TbCt.Location = new System.Drawing.Point(419, 167);
			this.TbCt.Name = "TbCt";
			this.TbCt.Size = new System.Drawing.Size(179, 20);
			this.TbCt.TabIndex = 54;
			// 
			// TbCL
			// 
			this.TbCL.Location = new System.Drawing.Point(419, 111);
			this.TbCL.Name = "TbCL";
			this.TbCL.Size = new System.Drawing.Size(179, 20);
			this.TbCL.TabIndex = 52;
			// 
			// TbRO
			// 
			this.TbRO.Location = new System.Drawing.Point(419, 63);
			this.TbRO.MaxLength = 11;
			this.TbRO.Name = "TbRO";
			this.TbRO.Size = new System.Drawing.Size(179, 20);
			this.TbRO.TabIndex = 50;
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label20.Dock = System.Windows.Forms.DockStyle.Top;
			this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label20.ForeColor = System.Drawing.Color.Transparent;
			this.label20.Location = new System.Drawing.Point(0, 0);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(698, 35);
			this.label20.TabIndex = 48;
			this.label20.Text = "Register Rework Modules";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label20.UseCompatibleTextRendering = true;
			// 
			// FormEng
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1406, 656);
			this.Controls.Add(this.panel1);
			this.Name = "FormEng";
			this.Text = "FormEng";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
