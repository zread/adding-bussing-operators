﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 10:45 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Xml;

using System.Data;
using System.Data.SqlClient;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormAddNew.
	/// </summary>
	public partial class FormAddNew : Form
	{
		
		private StringClass NewEntry = new StringClass();			
		List<string> AminusMarking = new List<string>();
		public FormAddNew()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
            DisplayData();
            //
            // TODO: Add constructor code after the InitializeComponent() call.
            //
        }
		private static FormAddNew _theSingleton;
		 public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FormAddNew
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
           
              
        }


	
		#region windons form
		//carton		



        private void bTadd_Click(object sender, EventArgs e)
        {
            string connectionstring = @"Data Source=10.127.32.47;Initial Catalog=CAPA01DB01;User ID=PowerDistribution;Password=Power123";

            string Names = textBox1.Text.ToString();
            if(string.IsNullOrEmpty(Names))
            {
                MessageBox.Show("Please enter user Name");
                return;
            }
            string[] NameList = Names.Split('\n');
            int i = 1, LastNum = 0;

            string sql = @"select max(id) from [Solderers] where id not in (800,900)";
            DataTable dt = Toolsclass.PullData(sql,connectionstring);
            if (dt != null && dt.Rows.Count > 0)
                LastNum = Convert.ToInt16(dt.Rows[0][0].ToString());
            string sqlinsert = "";
            foreach(string name in NameList)
            {
                int NumberInsert = 0;
                NumberInsert = i + LastNum;
                if(NumberInsert == 800 || NumberInsert == 900)
                {
                    NumberInsert = NumberInsert + 1;
                    i = i + 1;
                }

                sqlinsert = @"insert into solderers values ('{0}',{1})";
                sqlinsert = string.Format(sqlinsert, name, NumberInsert.ToString());

                Toolsclass.PutsData(sqlinsert, connectionstring);

                i++;                
            }

            textBox1.Text = "";
            DisplayData();
        }


        void DisplayData()
        {
            string connectionstring = @"Data Source=10.127.32.47;Initial Catalog=CAPA01DB01;User ID=PowerDistribution;Password=Power123";
            string sql = @"select * from [Solderers] order by ID desc";
            DataTable dt = Toolsclass.PullData(sql,connectionstring);
            dataGridView1.DataSource = dt;
        }
		
		#endregion
		
		
		
		
	}
}
