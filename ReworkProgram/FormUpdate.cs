﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 2:42 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Linq;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormUpdate.
	/// </summary>
	public partial class FormUpdate : Form
	{
		private string[] Eplcarton = {"TbCarton","TbRO","","",""};
		public FormUpdate()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		private static FormUpdate _theSingleton;
		 public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FormUpdate
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
           
              
        }
		void BtNext1Click(object sender, EventArgs e)
		{
			if(!CheckPanelData(PlCarton,Eplcarton))
				return;
//			if(CbStatus.SelectedIndex == 1 && string.IsNullOrEmpty(TbNotes.Text.ToString()))
//			{
//				MessageBox.Show("Please provide detailed Description!");
//				return;
//			}
			string strTemp = TbSN.Text.ToString();
		
			string[] sDataSet = strTemp.Split('\n');
			string[] NotInUse = new string[sDataSet.Length];
			string[] InUse = new string[sDataSet.Length];
			int NC= 0,IC = 0;
			
                    for (int i = 0; i < sDataSet.Length; i++)
                    {
                        if (string.IsNullOrEmpty(sDataSet[i]))
                    		continue;
               sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                if (CheckSnLocation(sDataSet[i])) {
                    		InUse[IC] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    		IC ++;
                    	}
                    	else
                    	{
                    		NotInUse[NC] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    		NC ++;
                    	}
                    	//sDataSet[i] =  sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                    }
			
            string Ltemp = string.Join(" ", NotInUse).Replace(' ','\r');
			if (!string.IsNullOrEmpty(Ltemp)) {
				DialogResult dialogResult = MessageBox.Show("Following Serial numbers cannot be processed, because they are not in rework area. \n"+ Ltemp + "\nClick ok to copy serial number into Clipboard","Warning",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning);
			if(dialogResult == DialogResult.OK)
				Clipboard.SetText(Ltemp);
			
			}      
                    
                    
                    
			int chk = UpdateDatabase(InUse,CbStatus.SelectedIndex,TbNotes.Text.ToString());
			if (chk > 0) {
				MessageBox.Show("update succesful!");
			}
			else
			{
				MessageBox.Show("Nothing has been modified!");
			}
			ClearALL();
			
		}
		
		bool CheckSnLocation(string sn)
		{
			string sql = @"select top 1 location from (
  							select ROW_NUMBER() over (partition by SN order by processTime) as r,* 
							from ReworkModuleLocation where sn = '{0}') as L order by r desc";
			sql = string.Format(sql,sn);
			DataTable dt = Toolsclass.PullData(sql);
			if(dt != null && dt.Rows.Count == 1)
				if (dt.Rows[0][0].ToString()  == "5") 
					return true;
			return false;
			
				
		}
		bool CheckPanelData(Panel pl, string[] exception)
		{
			foreach (Control ctl in pl.Controls) {
				if(string.IsNullOrEmpty(ctl.Text.ToString()) && !exception.Contains(ctl.Name.ToString()))
				{
					MessageBox.Show("Please Fill in the Entry of: "+ ctl.Name.ToString().Substring(2,ctl.Name.Length-2),"Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
					return false;
				}
			}
			return true;
		}
		void TbCarton_keyPress(object sender, KeyPressEventArgs e)
		{
			
			if(e.KeyChar == '\r')
			{
				TbSN.Clear();
				string sql = @"select sn from Rework_tracebility where BoxIn = '"+TbCarton.Text.ToString()+"'";
				DataTable dt = Toolsclass.PullData(sql);
				if(dt.Rows.Count > 0)
				{
					foreach (DataRow row in dt.Rows) {
						TbSN.Text += row[0].ToString()+ Environment.NewLine;
					}
				}
			}
		}
		int UpdateDatabase(string []sn, int status, string notes )
		{
			int ModuleStatus = -1;
			bool Delivered = false;
			bool FinishRework = false;
			notes = notes.Replace("\'","\""); //.Replace("\"", "");
			string sql = "update Rework_tracebility set ReworkNotes = '{0}', ModuleStatus = {2}, reworkdate = getdate() where sn in ('{1}') and ModuleStatus < 8 and ModuleStatus <> 1";
			switch (status) {
				case 0:
					ModuleStatus = 1;
					Delivered = true;
					//sql = @"update Rework_tracebility set ReworkNotes = '{0}' where sn in ('{1}') and ModuleStatus < 8 ";
					break;
				case 1:
					ModuleStatus = 3;
					break;
				case 2:
					ModuleStatus = 2;
					break;
				case 3:
					ModuleStatus = 7;					
					break;	
				case 4:
					sql = "update Rework_tracebility set ReworkNotes = '{0}', ModuleStatus = {2}, boxout = '{3}', reworkdate = getdate() where sn in ('{1}') and ModuleStatus < 8  and ModuleStatus <> 1";
					ModuleStatus = 8;
					Delivered = true;
					FinishRework = true;
					CbDT.SelectedIndex = MainForm.WH;
					break;			
				default:
					sql = @"update Rework_tracebility set ReworkNotes = '{0}' where sn in ('{1}') and ModuleStatus < 8  and ModuleStatus <> 1";
					break;
					
			}
			sql = string.Format(sql,notes,string.Join("','",sn),ModuleStatus,TbCarton2.Text.ToString());
			int i = Toolsclass.PutsData(sql);
			
			if (Delivered)
			{
				UpdateModuleLocation(sn, CbDT.SelectedIndex);
			}
			if(FinishRework)
			{
				UpdateReworkOrder(sn);
			}
			
//			if(!string.IsNullOrEmpty(TbRO.Text.ToString()))
//			{
//				sql = @"update Rework_tracebility set ReworkOrder = '{0}' where sn in ('{1}') and ModuleStatus < 8";
//				sql = string.Format(sql,TbRO.Text.ToString(),string.Join("','",sn));
//				Toolsclass.PutsData(sql);
//			}
			return i;
		}
		void UpdateReworkOrder(string[] sn )
		{
			string sql = @"update [ReworkOrderToSerial] set IsFinRework = 1 where SN in ('{0}')";
			sql = string.Format(sql,string.Join("','", sn));
			Toolsclass.PutsData(sql);			
		}
		void UpdateModuleLocation(string[] sn,int loc)
		{
			string sql =@"insert into ReworkModuleLocation values('{0}',{1},'{2}',getdate())";
			string Opt = FormLogin.usernm;
			foreach (string s in sn) {
				string sqlfinal = string.Format(sql,s,loc,Opt);
				Toolsclass.PutsData(sqlfinal);				
			}
			
		}
		void ClearALL()
		{
			TbCarton.Clear();
			TbSN.Clear();
			TbNotes.Clear();
			CbStatus.SelectedIndex = -1;
		}
		void U_HistoryClick(object sender, EventArgs e)
		{
			dataGridView1.Rows.Clear();
			string sql = "";
			if(string.IsNullOrEmpty(TB_SN.Text.ToString())&&!checkbox_date.Checked)
			{
				sql = @"select R.SN,BoxIn,ReworkOrder,ReceivingDate,ReworkOperator, 
				class = case when ModuleClass is null then 'A' 
				else ModuleClass end,
				Status1 = case 
				WHEN ModuleStatus = 0 then 'Module received'
				when ModuleStatus = 1 then 'Delivered to other department'
				WHEN ModuleStatus = 2 THEN 'Quailty check required'
				WHEN ModuleStatus = 3 THEN 'Engineering support required'
				WHEN ModuleStatus = 4 THEN 'QA Approved'
				WHEN ModuleStatus = 5 THEN 'QA Reject'
				WHEN ModuleStatus = 6 THEN 'Engineering Support Finished'
				WHEN ModuleStatus = 7 THEN 'Other Issue'
				WHEN ModuleStatus = 8 THEN 'Finished'
				END,
				boxout,
				ReworkNotes,
				QAComments,				
				ModuleLocation = case 
				when Location = 0 then 'Production Line'  
				when Location = 1 then 'LAB'
				when Location = 2 then 'MRB'
				when Location = 3 then 'Warehouse'
				when Location = 4 then 'Other'
				when Location = 5 then 'Rework'
				else 'Rework'
				End
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 ) as Q on R.SN collate SQL_Latin1_General_CP1_CI_AS = Q.SN
				 full join
				 (
				 select * from 
				  ( select row_number() over(partition by SN order by Processtime desc) as row,* from  ReworkModuleLocation)as b where row = 1
				 )
				 as Loc on R.SN collate SQL_Latin1_General_CP1_CI_AS = LOC.SN
				 WHERE  ModuleStatus < 8 ";
			}					
			else if(string.IsNullOrEmpty(TB_SN.Text.ToString())&&checkbox_date.Checked)
			{
				sql = @"select R.SN,BoxIn,ReworkOrder,ReceivingDate,ReworkOperator, 
				class = case when ModuleClass is null then 'A' 
				else ModuleClass end,
				Status1 = case 
				WHEN ModuleStatus = 0 then 'Module received'
				when ModuleStatus = 1 then 'Delivered to other department'
				WHEN ModuleStatus = 2 THEN 'Quailty check required'
				WHEN ModuleStatus = 3 THEN 'Engineering support required'
				WHEN ModuleStatus = 4 THEN 'QA Approved'
				WHEN ModuleStatus = 5 THEN 'QA Reject'
				WHEN ModuleStatus = 6 THEN 'Engineering Support Finished'
				WHEN ModuleStatus = 7 THEN 'Other Issue'
				WHEN ModuleStatus = 8 THEN 'Finished'
				END,
				boxout,
				ReworkNotes,	
				QAComments,
				ModuleLocation = case 
				when Location = 0 then 'Production Line'  
				when Location = 1 then 'LAB'
				when Location = 2 then 'MRB'
				when Location = 3 then 'Warehouse'
				when Location = 4 then 'Other'
				when Location = 5 then 'Rework'
				else 'Rework'
				End		
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 ) as Q on R.SN collate SQL_Latin1_General_CP1_CI_AS = Q.SN
				  full join
				 (
				 select * from 
				  ( select row_number() over(partition by SN order by Processtime desc) as row,* from  ReworkModuleLocation)as b where row = 1
				 )
				 as Loc on R.SN collate SQL_Latin1_General_CP1_CI_AS = LOC.SN 
				WHERE  ModuleStatus < 8and r.ReworkDate between '{0}' and '{1}'";
				sql = string.Format(sql,dateTimePicker1.Value.ToString(),dateTimePicker2.Value.ToString());
			}
			else if(!string.IsNullOrEmpty(TB_SN.Text.ToString())&&!checkbox_date.Checked)
			{
				sql = @"select R.SN,BoxIn,ReworkOrder,ReceivingDate,ReworkOperator, 
				class = case when ModuleClass is null then 'A' 
				else ModuleClass end,
				Status1 = case 
				WHEN ModuleStatus = 0 then 'Module received'
				when ModuleStatus = 1 then 'Delivered to other department'
				WHEN ModuleStatus = 2 THEN 'Quailty check required'
				WHEN ModuleStatus = 3 THEN 'Engineering support required'
				WHEN ModuleStatus = 4 THEN 'QA Approved'
				WHEN ModuleStatus = 5 THEN 'QA Reject'
				WHEN ModuleStatus = 6 THEN 'Engineering Support Finished'
				WHEN ModuleStatus = 7 THEN 'Other Issue'
				WHEN ModuleStatus = 8 THEN 'Finished'
				END,
				boxout,
				ReworkNotes,
				QAComments,
				ModuleLocation = case 
				when Location = 0 then 'Production Line'  
				when Location = 1 then 'LAB'
				when Location = 2 then 'MRB'
				when Location = 3 then 'Warehouse'
				when Location = 4 then 'Other'
				when Location = 5 then 'Rework'
				else 'Rework'
				End
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 ) as Q on R.SN collate SQL_Latin1_General_CP1_CI_AS = Q.SN 
				full join
				 (
				 select * from 
				  ( select row_number() over(partition by SN order by Processtime desc) as row,* from  ReworkModuleLocation)as b where row = 1
				 )
				 as Loc on R.SN collate SQL_Latin1_General_CP1_CI_AS = LOC.SN
 				WHERE  ModuleStatus < 8	and R.sn = '{0}'";
				sql = string.Format(sql,TB_SN.Text.ToString());
			}
			else
			{
				sql = @"select R.SN,BoxIn,ReworkOrder,ReceivingDate,ReworkOperator, 
				class = case when ModuleClass is null then 'A' 
				else ModuleClass end,
				Status1 = case 
				WHEN ModuleStatus = 0 then 'Module received'
				when ModuleStatus = 1 then 'Delivered to other department'
				WHEN ModuleStatus = 2 THEN 'Quailty check required'
				WHEN ModuleStatus = 3 THEN 'Engineering support required'
				WHEN ModuleStatus = 4 THEN 'QA Approved'
				WHEN ModuleStatus = 5 THEN 'QA Reject'
				WHEN ModuleStatus = 6 THEN 'Engineering Support Finished'
				WHEN ModuleStatus = 7 THEN 'Other Issue'
				WHEN ModuleStatus = 8 THEN 'Finished'
				END,
				boxout,
				ReworkNotes,
				QAComments,
				ModuleLocation = case 
				when Location = 0 then 'Production Line'  
				when Location = 1 then 'LAB'
				when Location = 2 then 'MRB'
				when Location = 3 then 'Warehouse'
				when Location = 4 then 'Other'
				when Location = 5 then 'Rework'
				else 'Rework'
				End
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 ) as Q on R.SN collate SQL_Latin1_General_CP1_CI_AS = Q.SN
				 full join
				 (
				 select * from 
				  ( select row_number() over(partition by SN order by Processtime desc) as row,* from  ReworkModuleLocation)as b where row = 1
				 )
				 as Loc on R.SN collate SQL_Latin1_General_CP1_CI_AS = LOC.SN
				WHERE  ModuleStatus < 8 and R.sn = '{0}' and r.ReworkDate between '{1}' and '{2}'";
				sql = string.Format(sql,TB_SN.Text.ToString(),dateTimePicker1.Value.ToString(),dateTimePicker2.Value.ToString());
			}
			
			DataTable dt = Toolsclass.PullData(sql);
			if(dt!= null && dt.Rows.Count > 0)
			foreach ( DataRow row in dt.Rows) {				
				dataGridView1.Rows.Insert(dataGridView1.RowCount, new object[] {row[0].ToString(),row[1].ToString(),row[2].ToString(),row[3].ToString(),row[4].ToString(),row[5].ToString(),row[6].ToString(),row[7].ToString(),row[8].ToString(),row[9].ToString(),row[10].ToString()});
		   }
			
		}
		void Checkbox_dateCheckedChanged(object sender, EventArgs e)
		{
			if(checkbox_date.Checked == false)
				{
					dateTimePicker1.Enabled = false;
					dateTimePicker2.Enabled = false;
					
				}
				else
				{
					dateTimePicker1.Enabled = true;
					dateTimePicker2.Enabled = true;
				}
		}
		void CbStatusSelectedIndexChanged(object sender, EventArgs e)
		{
				
				if (CbStatus.SelectedIndex == 3 || CbStatus.SelectedIndex == 1) 
				{
					Eplcarton[Eplcarton.Length -3] = "";
				}
				else
				{
					Eplcarton[Eplcarton.Length -3] = "TbNotes";
				}
			
				if (CbStatus.SelectedIndex == 4) {
					TbCarton2.Visible = true;
					label9.Visible = true;
					Eplcarton[Eplcarton.Length-1] = "";
				}
				else
				{
					TbCarton2.Visible = false;
					label9.Visible = false;
					Eplcarton[Eplcarton.Length-1] = "TbCarton2";
					TbCarton2.Clear();
				}
				
				if (CbStatus.SelectedIndex == 0) {
					LbDT.Visible = true;
					CbDT.Visible = true;
					Eplcarton[Eplcarton.Length-2] = "";					
				}
				else
				{
					LbDT.Visible = false;
					CbDT.Visible = false;
					CbDT.SelectedIndex = -1;
					Eplcarton[Eplcarton.Length-2] = "CbDT";	
				}
				
		}
		
	}
}
