﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 7/15/2016
 * Time: 2:32 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ReworkProgram
{
	partial class FormDefect
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
	
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDefect));
			this.label5 = new System.Windows.Forms.Label();
			this.textBox3 = new System.Windows.Forms.TextBox();
			this.cate1CB = new System.Windows.Forms.ComboBox();
			this.cate2CB = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label5
			// 
			this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label5.Location = new System.Drawing.Point(780, 100);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(206, 32);
			this.label5.TabIndex = 13;
			this.label5.Text = "Serial Number Entry";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// textBox3
			// 
			this.textBox3.Location = new System.Drawing.Point(780, 135);
			this.textBox3.Multiline = true;
			this.textBox3.Name = "textBox3";
			this.textBox3.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textBox3.Size = new System.Drawing.Size(206, 355);
			this.textBox3.TabIndex = 3;
			// 
			// cate1CB
			// 
			this.cate1CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cate1CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cate1CB.FormattingEnabled = true;
			this.cate1CB.Items.AddRange(new object[] {
			"B Class Rework",
			"Cleaning Rework",
			"Frame Rework",
			"Jbox Rework",
			"Label/Barcode Rework",
			"Laminate Rework",
			"No Rework",
			"Sorting"});
			this.cate1CB.Location = new System.Drawing.Point(109, 135);
			this.cate1CB.Name = "cate1CB";
			this.cate1CB.Size = new System.Drawing.Size(528, 26);
			this.cate1CB.TabIndex = 1;
			this.cate1CB.SelectedIndexChanged += new System.EventHandler(this.Cate1CBSelectedIndexChanged);
			// 
			// cate2CB
			// 
			this.cate2CB.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cate2CB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.cate2CB.FormattingEnabled = true;
			this.cate2CB.Location = new System.Drawing.Point(109, 231);
			this.cate2CB.Name = "cate2CB";
			this.cate2CB.Size = new System.Drawing.Size(528, 26);
			this.cate2CB.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label1.Location = new System.Drawing.Point(109, 100);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(222, 32);
			this.label1.TabIndex = 16;
			this.label1.Text = "First Category";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label2.Location = new System.Drawing.Point(109, 182);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(222, 32);
			this.label2.TabIndex = 17;
			this.label2.Text = "Second Category";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBox1
			// 
			this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.textBox1.Location = new System.Drawing.Point(109, 304);
			this.textBox1.MaxLength = 300;
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(528, 102);
			this.textBox1.TabIndex = 4;
			// 
			// label3
			// 
			this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label3.Location = new System.Drawing.Point(109, 269);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(222, 32);
			this.label3.TabIndex = 19;
			this.label3.Text = "Additional Comments";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.button1.Location = new System.Drawing.Point(280, 442);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(157, 48);
			this.button1.TabIndex = 5;
			this.button1.Text = "Create Entry";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// FormDefect
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(1406, 656);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textBox1);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.cate2CB);
			this.Controls.Add(this.cate1CB);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.textBox3);
			this.Name = "FormDefect";
			this.Padding = new System.Windows.Forms.Padding(3);
			this.Text = "FormDefect";
			this.ResumeLayout(false);
			this.PerformLayout();

		}
		
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBox3;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.ComboBox cate2CB;
		private System.Windows.Forms.ComboBox cate1CB;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		
		void Cate1CBSelectedIndexChanged(object sender, System.EventArgs e)
		{
			
			PopulateCategory2();
		}
	}
}
