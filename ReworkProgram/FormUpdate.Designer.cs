﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 2:42 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ReworkProgram
{
	partial class FormUpdate
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
	
		private System.Windows.Forms.Panel PlCarton;
		private System.Windows.Forms.ComboBox CbStatus;
		private System.Windows.Forms.Button BtNext1;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox TbCarton;
		private System.Windows.Forms.TextBox TbSN;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox TbNotes;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DataGridViewTextBoxColumn SN;
		private System.Windows.Forms.DataGridViewTextBoxColumn BOXID;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReworkReason;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReceivingDate;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReworkOperator;
		private System.Windows.Forms.DataGridViewTextBoxColumn ModuleClass;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReworkStatus;
		private System.Windows.Forms.CheckBox checkbox_date;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.DateTimePicker dateTimePicker2;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox TB_SN;
		private System.Windows.Forms.Button U_History;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox TbCarton2;
		private System.Windows.Forms.DataGridViewTextBoxColumn BOXOUT;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReworkNotes;
		private System.Windows.Forms.DataGridViewTextBoxColumn QualityNotes;
		private System.Windows.Forms.Label LbDT;
		private System.Windows.Forms.ComboBox CbDT;
		private System.Windows.Forms.DataGridViewTextBoxColumn ModuleLocation;
		
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUpdate));
			this.PlCarton = new System.Windows.Forms.Panel();
			this.CbDT = new System.Windows.Forms.ComboBox();
			this.LbDT = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.TbCarton2 = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.TbNotes = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.CbStatus = new System.Windows.Forms.ComboBox();
			this.BtNext1 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.TbCarton = new System.Windows.Forms.TextBox();
			this.TbSN = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BOXID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReworkReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReceivingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReworkOperator = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ModuleClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReworkStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BOXOUT = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReworkNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.QualityNotes = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ModuleLocation = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label5 = new System.Windows.Forms.Label();
			this.checkbox_date = new System.Windows.Forms.CheckBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.label8 = new System.Windows.Forms.Label();
			this.TB_SN = new System.Windows.Forms.TextBox();
			this.U_History = new System.Windows.Forms.Button();
			this.PlCarton.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// PlCarton
			// 
			this.PlCarton.BackColor = System.Drawing.SystemColors.Control;
			this.PlCarton.Controls.Add(this.CbDT);
			this.PlCarton.Controls.Add(this.LbDT);
			this.PlCarton.Controls.Add(this.label9);
			this.PlCarton.Controls.Add(this.TbCarton2);
			this.PlCarton.Controls.Add(this.label3);
			this.PlCarton.Controls.Add(this.TbNotes);
			this.PlCarton.Controls.Add(this.label20);
			this.PlCarton.Controls.Add(this.label2);
			this.PlCarton.Controls.Add(this.CbStatus);
			this.PlCarton.Controls.Add(this.BtNext1);
			this.PlCarton.Controls.Add(this.label4);
			this.PlCarton.Controls.Add(this.TbCarton);
			this.PlCarton.Controls.Add(this.TbSN);
			this.PlCarton.Controls.Add(this.label1);
			this.PlCarton.Location = new System.Drawing.Point(26, 90);
			this.PlCarton.Name = "PlCarton";
			this.PlCarton.Size = new System.Drawing.Size(487, 444);
			this.PlCarton.TabIndex = 12;
			// 
			// CbDT
			// 
			this.CbDT.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbDT.FormattingEnabled = true;
			this.CbDT.Items.AddRange(new object[] {
			"Production Line",
			"LAB",
			"MRB",
			"Warehouse",
			"Other"});
			this.CbDT.Location = new System.Drawing.Point(233, 109);
			this.CbDT.Name = "CbDT";
			this.CbDT.Size = new System.Drawing.Size(195, 21);
			this.CbDT.TabIndex = 56;
			this.CbDT.Visible = false;
			// 
			// LbDT
			// 
			this.LbDT.Location = new System.Drawing.Point(233, 85);
			this.LbDT.Name = "LbDT";
			this.LbDT.Size = new System.Drawing.Size(152, 22);
			this.LbDT.TabIndex = 55;
			this.LbDT.Text = "*Delivery to:";
			this.LbDT.Visible = false;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(20, 88);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(152, 22);
			this.label9.TabIndex = 51;
			this.label9.Text = "*Carton No After Rework";
			this.label9.Visible = false;
			// 
			// TbCarton2
			// 
			this.TbCarton2.Location = new System.Drawing.Point(20, 110);
			this.TbCarton2.Name = "TbCarton2";
			this.TbCarton2.Size = new System.Drawing.Size(196, 20);
			this.TbCarton2.TabIndex = 50;
			this.TbCarton2.Visible = false;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(233, 133);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(99, 23);
			this.label3.TabIndex = 49;
			this.label3.Text = "Rework Notes:";
			// 
			// TbNotes
			// 
			this.TbNotes.Location = new System.Drawing.Point(233, 159);
			this.TbNotes.MaxLength = 500;
			this.TbNotes.Multiline = true;
			this.TbNotes.Name = "TbNotes";
			this.TbNotes.Size = new System.Drawing.Size(195, 236);
			this.TbNotes.TabIndex = 48;
			// 
			// label20
			// 
			this.label20.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label20.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label20.ForeColor = System.Drawing.Color.Transparent;
			this.label20.Location = new System.Drawing.Point(0, 3);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(487, 35);
			this.label20.TabIndex = 47;
			this.label20.Text = "Updating rework status";
			this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label20.UseCompatibleTextRendering = true;
			// 
			// label2
			// 
			this.label2.BackColor = System.Drawing.SystemColors.Control;
			this.label2.Location = new System.Drawing.Point(233, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(149, 21);
			this.label2.TabIndex = 7;
			this.label2.Text = "*Status";
			// 
			// CbStatus
			// 
			this.CbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbStatus.FormattingEnabled = true;
			this.CbStatus.Items.AddRange(new object[] {
			"Delivered to other department",
			"Waiting for engineering support",
			"Waiting for QA",
			"Others",
			"In SAP"});
			this.CbStatus.Location = new System.Drawing.Point(233, 61);
			this.CbStatus.Name = "CbStatus";
			this.CbStatus.Size = new System.Drawing.Size(195, 21);
			this.CbStatus.TabIndex = 6;
			this.CbStatus.SelectedIndexChanged += new System.EventHandler(this.CbStatusSelectedIndexChanged);
			// 
			// BtNext1
			// 
			this.BtNext1.Location = new System.Drawing.Point(379, 415);
			this.BtNext1.Name = "BtNext1";
			this.BtNext1.Size = new System.Drawing.Size(103, 23);
			this.BtNext1.TabIndex = 5;
			this.BtNext1.Text = "Submit";
			this.BtNext1.UseVisualStyleBackColor = true;
			this.BtNext1.Click += new System.EventHandler(this.BtNext1Click);
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(20, 41);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(84, 22);
			this.label4.TabIndex = 4;
			this.label4.Text = "Carton No:";
			// 
			// TbCarton
			// 
			this.TbCarton.Location = new System.Drawing.Point(20, 63);
			this.TbCarton.Name = "TbCarton";
			this.TbCarton.Size = new System.Drawing.Size(196, 20);
			this.TbCarton.TabIndex = 0;
			this.TbCarton.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TbCarton_keyPress);
			// 
			// TbSN
			// 
			this.TbSN.Location = new System.Drawing.Point(20, 159);
			this.TbSN.Multiline = true;
			this.TbSN.Name = "TbSN";
			this.TbSN.Size = new System.Drawing.Size(195, 236);
			this.TbSN.TabIndex = 2;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(20, 133);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(99, 23);
			this.label1.TabIndex = 3;
			this.label1.Text = "*SN:";
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.SN,
			this.BOXID,
			this.ReworkReason,
			this.ReceivingDate,
			this.ReworkOperator,
			this.ModuleClass,
			this.ReworkStatus,
			this.BOXOUT,
			this.ReworkNotes,
			this.QualityNotes,
			this.ModuleLocation});
			this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridView1.Location = new System.Drawing.Point(0, 38);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.ReadOnly = true;
			this.dataGridView1.Size = new System.Drawing.Size(749, 406);
			this.dataGridView1.TabIndex = 13;
			// 
			// SN
			// 
			this.SN.HeaderText = "SN";
			this.SN.Name = "SN";
			this.SN.ReadOnly = true;
			this.SN.Width = 150;
			// 
			// BOXID
			// 
			this.BOXID.HeaderText = "BOXIN";
			this.BOXID.Name = "BOXID";
			this.BOXID.ReadOnly = true;
			// 
			// ReworkReason
			// 
			this.ReworkReason.HeaderText = "ReworkOrder";
			this.ReworkReason.Name = "ReworkReason";
			this.ReworkReason.ReadOnly = true;
			this.ReworkReason.Width = 150;
			// 
			// ReceivingDate
			// 
			this.ReceivingDate.HeaderText = "ReceivingDate";
			this.ReceivingDate.Name = "ReceivingDate";
			this.ReceivingDate.ReadOnly = true;
			// 
			// ReworkOperator
			// 
			this.ReworkOperator.HeaderText = "ReworkOperator";
			this.ReworkOperator.Name = "ReworkOperator";
			this.ReworkOperator.ReadOnly = true;
			// 
			// ModuleClass
			// 
			this.ModuleClass.HeaderText = "ModuleClass";
			this.ModuleClass.Name = "ModuleClass";
			this.ModuleClass.ReadOnly = true;
			// 
			// ReworkStatus
			// 
			this.ReworkStatus.HeaderText = "ReworkStatus";
			this.ReworkStatus.Name = "ReworkStatus";
			this.ReworkStatus.ReadOnly = true;
			// 
			// BOXOUT
			// 
			this.BOXOUT.HeaderText = "BOXOUT";
			this.BOXOUT.Name = "BOXOUT";
			this.BOXOUT.ReadOnly = true;
			// 
			// ReworkNotes
			// 
			this.ReworkNotes.HeaderText = "ReworkNotes";
			this.ReworkNotes.Name = "ReworkNotes";
			this.ReworkNotes.ReadOnly = true;
			// 
			// QualityNotes
			// 
			this.QualityNotes.HeaderText = "QualityNotes";
			this.QualityNotes.Name = "QualityNotes";
			this.QualityNotes.ReadOnly = true;
			// 
			// ModuleLocation
			// 
			this.ModuleLocation.HeaderText = "ModuleLocation";
			this.ModuleLocation.Name = "ModuleLocation";
			this.ModuleLocation.ReadOnly = true;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.dataGridView1);
			this.panel1.Controls.Add(this.label5);
			this.panel1.Location = new System.Drawing.Point(519, 90);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(749, 444);
			this.panel1.TabIndex = 14;
			// 
			// label5
			// 
			this.label5.BackColor = System.Drawing.Color.DarkSeaGreen;
			this.label5.Dock = System.Windows.Forms.DockStyle.Top;
			this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.label5.ForeColor = System.Drawing.Color.Transparent;
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(749, 38);
			this.label5.TabIndex = 50;
			this.label5.Text = "Query Rework Modules";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.label5.UseCompatibleTextRendering = true;
			// 
			// checkbox_date
			// 
			this.checkbox_date.Location = new System.Drawing.Point(605, 562);
			this.checkbox_date.Name = "checkbox_date";
			this.checkbox_date.Size = new System.Drawing.Size(23, 24);
			this.checkbox_date.TabIndex = 58;
			this.checkbox_date.UseVisualStyleBackColor = true;
			this.checkbox_date.CheckedChanged += new System.EventHandler(this.Checkbox_dateCheckedChanged);
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(919, 567);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(38, 20);
			this.label6.TabIndex = 57;
			this.label6.Text = "To:";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(644, 567);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(38, 17);
			this.label7.TabIndex = 56;
			this.label7.Text = "From:";
			// 
			// dateTimePicker2
			// 
			this.dateTimePicker2.Enabled = false;
			this.dateTimePicker2.Location = new System.Drawing.Point(963, 566);
			this.dateTimePicker2.Name = "dateTimePicker2";
			this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
			this.dateTimePicker2.TabIndex = 55;
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Enabled = false;
			this.dateTimePicker1.Location = new System.Drawing.Point(688, 566);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
			this.dateTimePicker1.TabIndex = 54;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(368, 567);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(38, 20);
			this.label8.TabIndex = 53;
			this.label8.Text = "SN";
			// 
			// TB_SN
			// 
			this.TB_SN.Location = new System.Drawing.Point(430, 569);
			this.TB_SN.Name = "TB_SN";
			this.TB_SN.Size = new System.Drawing.Size(149, 20);
			this.TB_SN.TabIndex = 52;
			// 
			// U_History
			// 
			this.U_History.Location = new System.Drawing.Point(1193, 567);
			this.U_History.Name = "U_History";
			this.U_History.Size = new System.Drawing.Size(79, 23);
			this.U_History.TabIndex = 51;
			this.U_History.Text = "ShowHistory";
			this.U_History.UseVisualStyleBackColor = true;
			this.U_History.Click += new System.EventHandler(this.U_HistoryClick);
			// 
			// FormUpdate
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(1362, 656);
			this.Controls.Add(this.checkbox_date);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.dateTimePicker2);
			this.Controls.Add(this.dateTimePicker1);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.TB_SN);
			this.Controls.Add(this.U_History);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.PlCarton);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormUpdate";
			this.Text = "FormUpdate";
			this.PlCarton.ResumeLayout(false);
			this.PlCarton.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}
	}
}
