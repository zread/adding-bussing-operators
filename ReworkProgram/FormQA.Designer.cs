﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 4:11 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ReworkProgram
{
	partial class FormQA
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.DataGridView dataGridView1;
		private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
		private System.Windows.Forms.DataGridViewTextBoxColumn SN;
		private System.Windows.Forms.DataGridViewTextBoxColumn BOXID;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReworkReason;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReceivingDate;
		private System.Windows.Forms.DataGridViewTextBoxColumn ReworkOperator;
		private System.Windows.Forms.DataGridViewTextBoxColumn ModuleClass;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.CheckBox checkbox_date;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dateTimePicker2;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox TB_SN;
		private System.Windows.Forms.Button U_History;
		private System.Windows.Forms.Button Btselect;
		private System.Windows.Forms.Button BtReject;
		private System.Windows.Forms.Button Bt_Approve;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox TbQA;
		private System.Windows.Forms.Button BtClear;
		private System.Windows.Forms.ComboBox CbGrade;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.ComboBox Cb_Defect;
		private System.Windows.Forms.TextBox TbQOperator;
		private System.Windows.Forms.TextBox TbROperator;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox CbQOperator;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.ComboBox CbROperator;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormQA));
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.dataGridView1 = new System.Windows.Forms.DataGridView();
			this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
			this.SN = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.BOXID = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReworkReason = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReceivingDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ReworkOperator = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.ModuleClass = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.panel1 = new System.Windows.Forms.Panel();
			this.CbGrade = new System.Windows.Forms.ComboBox();
			this.label8 = new System.Windows.Forms.Label();
			this.TbQOperator = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.TbROperator = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.BtClear = new System.Windows.Forms.Button();
			this.CbQOperator = new System.Windows.Forms.ComboBox();
			this.Cb_Defect = new System.Windows.Forms.ComboBox();
			this.label10 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.CbROperator = new System.Windows.Forms.ComboBox();
			this.TbQA = new System.Windows.Forms.TextBox();
			this.Btselect = new System.Windows.Forms.Button();
			this.BtReject = new System.Windows.Forms.Button();
			this.Bt_Approve = new System.Windows.Forms.Button();
			this.checkbox_date = new System.Windows.Forms.CheckBox();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.label1 = new System.Windows.Forms.Label();
			this.TB_SN = new System.Windows.Forms.TextBox();
			this.U_History = new System.Windows.Forms.Button();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel2
			// 
			this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
			this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(1362, 84);
			this.panel2.TabIndex = 1;
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.panel4);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(72, 656);
			this.panel3.TabIndex = 1;
			// 
			// panel4
			// 
			this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel4.Location = new System.Drawing.Point(0, 132);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(72, 524);
			this.panel4.TabIndex = 2;
			// 
			// dataGridView1
			// 
			this.dataGridView1.AllowUserToAddRows = false;
			this.dataGridView1.AllowUserToDeleteRows = false;
			this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
			this.Column1,
			this.SN,
			this.BOXID,
			this.ReworkReason,
			this.ReceivingDate,
			this.ReworkOperator,
			this.ModuleClass});
			this.dataGridView1.Location = new System.Drawing.Point(30, 84);
			this.dataGridView1.Name = "dataGridView1";
			this.dataGridView1.Size = new System.Drawing.Size(997, 432);
			this.dataGridView1.TabIndex = 0;
			// 
			// Column1
			// 
			this.Column1.HeaderText = "Select";
			this.Column1.Name = "Column1";
			this.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.Column1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
			this.Column1.Width = 50;
			// 
			// SN
			// 
			this.SN.HeaderText = "SN";
			this.SN.Name = "SN";
			this.SN.Width = 150;
			// 
			// BOXID
			// 
			this.BOXID.HeaderText = "BOXID";
			this.BOXID.Name = "BOXID";
			this.BOXID.Width = 150;
			// 
			// ReworkReason
			// 
			this.ReworkReason.HeaderText = "ReworkReason";
			this.ReworkReason.Name = "ReworkReason";
			this.ReworkReason.Width = 300;
			// 
			// ReceivingDate
			// 
			this.ReceivingDate.HeaderText = "ReceivingDate";
			this.ReceivingDate.Name = "ReceivingDate";
			// 
			// ReworkOperator
			// 
			this.ReworkOperator.HeaderText = "ReworkOperator";
			this.ReworkOperator.Name = "ReworkOperator";
			// 
			// ModuleClass
			// 
			this.ModuleClass.HeaderText = "ModuleClass";
			this.ModuleClass.Name = "ModuleClass";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.CbGrade);
			this.panel1.Controls.Add(this.label8);
			this.panel1.Controls.Add(this.TbQOperator);
			this.panel1.Controls.Add(this.label7);
			this.panel1.Controls.Add(this.TbROperator);
			this.panel1.Controls.Add(this.label9);
			this.panel1.Controls.Add(this.BtClear);
			this.panel1.Controls.Add(this.CbQOperator);
			this.panel1.Controls.Add(this.Cb_Defect);
			this.panel1.Controls.Add(this.label10);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.CbROperator);
			this.panel1.Controls.Add(this.TbQA);
			this.panel1.Controls.Add(this.Btselect);
			this.panel1.Controls.Add(this.BtReject);
			this.panel1.Controls.Add(this.Bt_Approve);
			this.panel1.Controls.Add(this.checkbox_date);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.dateTimePicker2);
			this.panel1.Controls.Add(this.dateTimePicker1);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.TB_SN);
			this.panel1.Controls.Add(this.U_History);
			this.panel1.Controls.Add(this.dataGridView1);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(1362, 656);
			this.panel1.TabIndex = 0;
			// 
			// CbGrade
			// 
			this.CbGrade.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbGrade.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CbGrade.FormattingEnabled = true;
			this.CbGrade.Items.AddRange(new object[] {
			"A",
			"A-",
			"B",
			"F"});
			this.CbGrade.Location = new System.Drawing.Point(1174, 227);
			this.CbGrade.Name = "CbGrade";
			this.CbGrade.Size = new System.Drawing.Size(58, 28);
			this.CbGrade.TabIndex = 19;
			// 
			// label8
			// 
			this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label8.Location = new System.Drawing.Point(1038, 263);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(142, 20);
			this.label8.TabIndex = 58;
			this.label8.Text = "Defect Category";
			// 
			// TbQOperator
			// 
			this.TbQOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TbQOperator.Location = new System.Drawing.Point(1174, 176);
			this.TbQOperator.Name = "TbQOperator";
			this.TbQOperator.Size = new System.Drawing.Size(164, 26);
			this.TbQOperator.TabIndex = 14;
			// 
			// label7
			// 
			this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label7.Location = new System.Drawing.Point(1038, 227);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(85, 20);
			this.label7.TabIndex = 18;
			this.label7.Text = "*Module Class";
			// 
			// TbROperator
			// 
			this.TbROperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.TbROperator.Location = new System.Drawing.Point(1174, 123);
			this.TbROperator.Name = "TbROperator";
			this.TbROperator.Size = new System.Drawing.Size(164, 26);
			this.TbROperator.TabIndex = 11;
			// 
			// label9
			// 
			this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label9.Location = new System.Drawing.Point(1038, 154);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(89, 17);
			this.label9.TabIndex = 15;
			this.label9.Text = "*Quality Operator";
			// 
			// BtClear
			// 
			this.BtClear.Location = new System.Drawing.Point(1107, 547);
			this.BtClear.Name = "BtClear";
			this.BtClear.Size = new System.Drawing.Size(79, 25);
			this.BtClear.TabIndex = 56;
			this.BtClear.Text = "Clear";
			this.BtClear.UseVisualStyleBackColor = true;
			this.BtClear.Click += new System.EventHandler(this.BtClearClick);
			// 
			// CbQOperator
			// 
			this.CbQOperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbQOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CbQOperator.FormattingEnabled = true;
			this.CbQOperator.Items.AddRange(new object[] {
			"",
			"Inspection by",
			"Double-check by"});
			this.CbQOperator.Location = new System.Drawing.Point(1038, 181);
			this.CbQOperator.Name = "CbQOperator";
			this.CbQOperator.Size = new System.Drawing.Size(129, 21);
			this.CbQOperator.TabIndex = 13;
			// 
			// Cb_Defect
			// 
			this.Cb_Defect.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.Cb_Defect.FormattingEnabled = true;
			this.Cb_Defect.Items.AddRange(new object[] {
			"",
			"Backsheet_Defect",
			"Backsheet_or_EVA_-_Blue_Tape",
			"Backsheet_Slit_Defects",
			"Backsheet_not_covering_glass",
			"Broken_Laminates_or_Modules",
			"Busbar_Showing",
			"Bubbles",
			"Bussbar_Defects",
			"Cell_damage",
			"Cell_Spacing",
			"Cosmetic_Defects",
			"Engineering_or_Lab_Analysis",
			"Final_Test_Failures",
			"Foreign_Material",
			"Frame_Joint",
			"Frame_Damage_or_Defect",
			"Overbaked_or_Underbaked_laminates",
			"Part_Orientation",
			"Product_Configuration",
			"Ribbon_Misaligned",
			"Ribbon_Twisted",
			"Scratch_on_Glass",
			"Serialization_and_Barcodes",
			"Silicone_or_Adhesive_Defects",
			"Solder_Splatter",
			"Supplier_Defects",
			"Uncut_Ribbon_or_Busbar",
			"Other"});
			this.Cb_Defect.Location = new System.Drawing.Point(1039, 286);
			this.Cb_Defect.Name = "Cb_Defect";
			this.Cb_Defect.Size = new System.Drawing.Size(208, 21);
			this.Cb_Defect.TabIndex = 57;
			// 
			// label10
			// 
			this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label10.Location = new System.Drawing.Point(1033, 98);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(94, 24);
			this.label10.TabIndex = 12;
			this.label10.Text = "*Rework Operator";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(1041, 310);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(82, 25);
			this.label4.TabIndex = 55;
			this.label4.Text = "QA Comments";
			// 
			// CbROperator
			// 
			this.CbROperator.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.CbROperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.CbROperator.FormattingEnabled = true;
			this.CbROperator.Items.AddRange(new object[] {
			"",
			"Rwk by",
			"Sort by",
			"No Defects Found by"});
			this.CbROperator.Location = new System.Drawing.Point(1038, 125);
			this.CbROperator.Name = "CbROperator";
			this.CbROperator.Size = new System.Drawing.Size(129, 21);
			this.CbROperator.TabIndex = 10;
			// 
			// TbQA
			// 
			this.TbQA.Location = new System.Drawing.Point(1041, 338);
			this.TbQA.Multiline = true;
			this.TbQA.Name = "TbQA";
			this.TbQA.Size = new System.Drawing.Size(297, 138);
			this.TbQA.TabIndex = 54;
			// 
			// Btselect
			// 
			this.Btselect.Location = new System.Drawing.Point(1002, 547);
			this.Btselect.Name = "Btselect";
			this.Btselect.Size = new System.Drawing.Size(79, 25);
			this.Btselect.TabIndex = 53;
			this.Btselect.Text = "Select All";
			this.Btselect.UseVisualStyleBackColor = true;
			this.Btselect.Click += new System.EventHandler(this.BtselectClick);
			// 
			// BtReject
			// 
			this.BtReject.Location = new System.Drawing.Point(1251, 491);
			this.BtReject.Name = "BtReject";
			this.BtReject.Size = new System.Drawing.Size(75, 25);
			this.BtReject.TabIndex = 52;
			this.BtReject.Text = "Reject";
			this.BtReject.UseVisualStyleBackColor = true;
			this.BtReject.Click += new System.EventHandler(this.BtRejectClick);
			// 
			// Bt_Approve
			// 
			this.Bt_Approve.Location = new System.Drawing.Point(1052, 491);
			this.Bt_Approve.Name = "Bt_Approve";
			this.Bt_Approve.Size = new System.Drawing.Size(75, 25);
			this.Bt_Approve.TabIndex = 51;
			this.Bt_Approve.Text = "Approve";
			this.Bt_Approve.UseVisualStyleBackColor = true;
			this.Bt_Approve.Click += new System.EventHandler(this.Bt_ApproveClick);
			// 
			// checkbox_date
			// 
			this.checkbox_date.Location = new System.Drawing.Point(305, 543);
			this.checkbox_date.Name = "checkbox_date";
			this.checkbox_date.Size = new System.Drawing.Size(23, 24);
			this.checkbox_date.TabIndex = 50;
			this.checkbox_date.UseVisualStyleBackColor = true;
			this.checkbox_date.CheckedChanged += new System.EventHandler(this.Checkbox_dateCheckedChanged);
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(619, 548);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(38, 20);
			this.label3.TabIndex = 49;
			this.label3.Text = "To:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(344, 548);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(38, 17);
			this.label2.TabIndex = 48;
			this.label2.Text = "From:";
			// 
			// dateTimePicker2
			// 
			this.dateTimePicker2.Enabled = false;
			this.dateTimePicker2.Location = new System.Drawing.Point(663, 547);
			this.dateTimePicker2.Name = "dateTimePicker2";
			this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
			this.dateTimePicker2.TabIndex = 47;
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Enabled = false;
			this.dateTimePicker1.Location = new System.Drawing.Point(388, 547);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(200, 20);
			this.dateTimePicker1.TabIndex = 46;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(68, 548);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 20);
			this.label1.TabIndex = 45;
			this.label1.Text = "SN";
			// 
			// TB_SN
			// 
			this.TB_SN.Location = new System.Drawing.Point(112, 545);
			this.TB_SN.Multiline = true;
			this.TB_SN.Name = "TB_SN";
			this.TB_SN.Size = new System.Drawing.Size(149, 81);
			this.TB_SN.TabIndex = 44;
			// 
			// U_History
			// 
			this.U_History.Location = new System.Drawing.Point(893, 548);
			this.U_History.Name = "U_History";
			this.U_History.Size = new System.Drawing.Size(79, 23);
			this.U_History.TabIndex = 43;
			this.U_History.Text = "ShowHistory";
			this.U_History.UseVisualStyleBackColor = true;
			this.U_History.Click += new System.EventHandler(this.U_HistoryClick);
			// 
			// FormQA
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
			this.ClientSize = new System.Drawing.Size(1362, 656);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormQA";
			this.Text = "FormQA";
			this.panel3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);

		}
	}
}
