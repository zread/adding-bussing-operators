﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 7/15/2016
 * Time: 2:32 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormDefect.
	/// </summary>
	public partial class FormDefect : Form
	{
		public FormDefect()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			if(Toolsclass.Isadmin(FormLogin.usernm))
				cate1CB.DropDownStyle = ComboBoxStyle.DropDown;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}

		
		private static FormDefect theSingleton = null;
		
		public static void Instance(Form fm)
        {
            if (null == theSingleton || theSingleton.IsDisposed)
            {
                theSingleton = new FormDefect();
                theSingleton.MdiParent = fm;
                theSingleton.WindowState = FormWindowState.Maximized;
                theSingleton.Show();
            }
            else
            {
                theSingleton.Activate();
                if(theSingleton.WindowState == FormWindowState.Minimized)
                {
                	theSingleton.WindowState = FormWindowState.Maximized;
                }
            }
        }
		
		
			public void PopulateCategory2(){
			cate2CB.Items.Clear();
			if(cate1CB.Text.ToString() == "B Class Rework"){
				cate2CB.Items.AddRange(new object[] {
									"Backsheet Scratch. Rwk Done. B Class",
									"Glass Scratch. B Class",
									"MRB Review"});
			}
			else if(cate1CB.Text.ToString() == "Cleaning Rework"){
				this.cate2CB.Items.AddRange(new object[] {
									"Cleaning Issue. Cleaned. Rwk Done",
									"EVA on Glass.  Cleaned. Rwk Done",									
									"Foreign Object. Removed. Rwk Done",
									"Silicon Cleaning Issue. Cleaned. Rwk Done",
									"Trimming Under Frame. Removed. Rwk Done",
				                    "Glass Scratch. Cleaned. Rwk Done"});
			}
			else if(cate1CB.Text.ToString() == "Frame Rework"){
				this.cate2CB.Items.AddRange(new object[] {
									"Cross Bar Missing. Installed. Rwk Done",
									"Cross Bar Damage. Changed. Rwk Done",									
									"Frame to Busbar <2mm. Reframed. Rwk Done",
									"Frame to Cell <2mm. Reframed. Rwk Done",
									"Long Frame Damage. Changed. Rwk Done",
									"L Frame & Screw Damage. Changed. Rwk Done",									
									"L & S Frame Damage. Changed. Rwk Done",
									"Screw Damage. Frame Changed. Rwk Done",									
									"Screw Damage. Screw Changed. Rwk Done",
									"Short Frame Damage. Changed. Rwk Done",
									"S Frame & Screw Damage. Changed. Rwk Done",
				                    "No Silicon in Rail. Frame Changed. Rwk Done"});
			}
			else if(cate1CB.Text.ToString() == "Jbox Rework"){
				this.cate2CB.Items.AddRange(new object[] {
									"Jbox Silicon Overfill. Cleaned. Rwk Done",
									"Jbox Silicon Underfill. Filled. Rwk Done",									
									"Missing Jbox Lid. Installed. Rwk Done",
									"Uncured Jbox Silicon. Repotted. Rwk Done",
									"Unpotted Jbox Silicon. Potted. Rwk Done",
									"Unsoldered Busbars. Soldered. Rwk Done"});
			}
			else if(cate1CB.Text.ToString() == "Label/Barcode Rework"){
				this.cate2CB.Items.AddRange(new object[] {
									"Duplicate Barcode. Changed. Rwk Done",
				                    "Barcode Label Issue. Corrected. Rwk Done",
									"Frame Label Issue. Corrected. Rwk Done",
									"Power Label Issue. Corrected. Rwk Done",
									"Frame and Power Label Issue. Corrected. Rwk Done",
									"Missing Flash Data. Pasan Retest Completed. Rwk Done "});
			}
			else if(cate1CB.Text.ToString() == "Laminate Rework"){
				this.cate2CB.Items.AddRange(new object[] {									
									"Foreign Object in Laminate. Removed. Rwk Done"});
			}
			else if(cate1CB.Text.ToString() == "No Rework"){
				this.cate2CB.Items.AddRange(new object[] {									
									"No Defects Found",
				                    "Meets “A” Class Criteria",
									"Reported Defect Not Found"});
			}
			else if(cate1CB.Text.ToString() == "Sorting"){
				this.cate2CB.Items.AddRange(new object[] {
				                    "Sort. Suspect Backsheet Scratch",
									"Sort. Suspect Frame Scratch",									
									"Sort. Suspect Glass Scratch",
				                    "Sort. Suspect Incorrect Jbox",
				                    "Sort. Suspect Unsoldered Busbars"});
			}
			
				
			
		}
		
		void Button1Click(object sender, EventArgs e)
		{

			
			if(string.IsNullOrEmpty(cate1CB.Text))
			if(cate1CB.SelectedItem == null || cate2CB.SelectedItem == null){
				MessageBox.Show("Please ensure that both category boxes have been filled in.");
				return;
			}
			
			if(string.IsNullOrEmpty(textBox3.Text.Trim())){
				MessageBox.Show("A serial number is required.");
				return;
			}
			
			string strTemp = textBox3.Text;
            string[] sDataSet = strTemp.Split('\n');
            for (int i = 0; i < sDataSet.Length; i++)
            {
                sDataSet[i] = sDataSet[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
                if (sDataSet[i] != null && sDataSet[i] != "")
                    batchGrade(sDataSet[i]);
            }
            textBox3.Clear();
            cate1CB.SelectedItem = null;
            cate2CB.SelectedItem = null;
            textBox1.Clear();
           
		}
		
		private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            sql = string.Format(sql, sSerialNumber);
            SqlDataReader sdr = Toolsclass.GetDataReader(sql);
            if(sdr != null && sdr.Read())
            {
            	return false;
            }
            sdr.Close();
            sdr = null;
            return true;
        }
		
		public void batchGrade(string sn){
			string category = "";
			category = cate2CB.Text.ToString();

			
			if(sn.Length != 13 && sn.Length != 14)
			{
				MessageBox.Show("Serial number "+sn+" does not match the correct length.");
				return;
			}			
			
            if (!isPacked(sn))
            {
            	MessageBox.Show("Module "+sn+" has already been packed.");
                return;
            }
                  
            string sql = "INSERT INTO Rework VALUES ('"+sn+"', '"+category+"', '"+textBox1.Text.ToString()+"', getdate())";
            Toolsclass.PutsData(sql,Toolsclass.GetK3Connection());
            
            sql = "INSERT INTO QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, remark) VALUES ('"+sn+"', 'A-', getdate(), '"+FormLogin.usernm+"', '', '"+category+"')";
             Toolsclass.PutsData(sql,Toolsclass.GetK3Connection());
		}

		
	}
}
