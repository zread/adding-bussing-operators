﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 4:11 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data;
using System.Linq;
using System.Collections.Generic;

namespace ReworkProgram
{
	/// <summary>
	/// Description of FormQA.
	/// </summary>
	public partial class FormQA : Form
	{
		public FormQA()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
			private static FormQA _theSingleton;
		 public static void Instance(Form fm)
        {
            if (null == _theSingleton || _theSingleton.IsDisposed)
            {
                _theSingleton = new FormQA
                {
                    MdiParent = fm,
                    WindowState = FormWindowState.Maximized
                };
                _theSingleton.Show();
            }
            else
            {
                _theSingleton.Activate();
                if (_theSingleton.WindowState == FormWindowState.Minimized)
                    _theSingleton.WindowState = FormWindowState.Maximized;
            }
                         
        }
		void U_HistoryClick(object sender, EventArgs e)
		{
			dataGridView1.Rows.Clear();
			string sql = "";
			string temp = TB_SN.Text.ToString();
			string[] strmp = temp.Split('\n');
			for( int i= 0; i<strmp.Length;i++)
				strmp[i] = strmp[i].Replace('\n', ' ').Replace('\r', ' ').Trim();
			
			strmp = GetRidOfNull(strmp);
			
			string Stemp = string.Join(" ", strmp).Replace(' ','\r');
			if(string.IsNullOrEmpty(Stemp)&&!checkbox_date.Checked)
			{
//				sql = @"select R.SN,BoxIn,ReworkComments,ReceivingDate,ReworkOperator, 
//				class = case when  ModuleClass is null then 'A' 
//				else ModuleClass end
//				from Rework_tracebility as R Full join CSILabelPrintDB.dbo.QualityJudge as Q on R.SN= Q.SN WHERE  ModuleStatus = 2 ";
				
				sql = @"  select R.SN,BoxIn,Q.Remark,ReceivingDate,
				Op = case when Q.Operator is null then ReworkOperator
				else Q.Operator end,
				class = case when  ModuleClass is null then 'A' 
				else ModuleClass end
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 )
				 as Q on R.SN collate Latin1_General_CI_AS = Q.SN WHERE  ModuleStatus = 2";
			}					
			else if(string.IsNullOrEmpty(Stemp)&&checkbox_date.Checked)
			{
//				sql = @"select R.SN,BoxIn,ReworkComments,ReceivingDate,ReworkOperator, 
//				class = case when ModuleClass is null then 'A' 
//				else ModuleClass end
//				from Rework_tracebility as R Full join CSILabelPrintDB.dbo.QualityJudge as Q on R.SN= Q.SN WHERE  ModuleStatus = 2
//				and r.ReworkDate between '{0}' and '{1}'";
				
				sql = @" select R.SN,BoxIn,Q.Remark,ReceivingDate,
				Op = case when Q.Operator is null then ReworkOperator
				else Q.Operator end, 
				class = case when  ModuleClass is null then 'A' 
				else ModuleClass end
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 )
				 as Q on R.SN collate Latin1_General_CI_AS = Q.SN WHERE  ModuleStatus = 2 and r.ReworkDate between '{0}' and '{1}' ";
				sql = string.Format(sql,dateTimePicker1.Value.ToString(),dateTimePicker2.Value.ToString());
			}
			else if(!string.IsNullOrEmpty(Stemp)&&!checkbox_date.Checked)
			{
//				sql = @"select R.SN,BoxIn,ReworkComments,ReceivingDate,ReworkOperator, 
//				class = case when ModuleClass is null then 'A' 
//				else ModuleClass end
//				from Rework_tracebility as R Full join CSILabelPrintDB.dbo.QualityJudge as Q on R.SN= Q.SN WHERE  ModuleStatus = 2 
//				and R.sn = '{0}'";
				
				sql = @"select R.SN,BoxIn,Q.Remark,ReceivingDate,
				Op = case when Q.Operator is null then ReworkOperator
				else Q.Operator end,
				class = case when  ModuleClass is null then 'A' 
				else ModuleClass end
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 )
				 as Q on R.SN collate Latin1_General_CI_AS = Q.SN WHERE  ModuleStatus = 2
				and R.sn in ('{0}')";			
				
				sql = string.Format(sql,string.Join("','",strmp));
			}
			else
			{
//				sql = @"select R.SN,BoxIn,ReworkComments,ReceivingDate,ReworkOperator, 
//				class = case when ModuleClass is null then 'A' 
//				else ModuleClass end
//				from Rework_tracebility as R Full join CSILabelPrintDB.dbo.QualityJudge as Q on R.SN= Q.SN WHERE  ModuleStatus = 2
//				and R.sn = '{0}' and r.ReworkDate between '{1}' and '{2}'";
				sql = @"select R.SN,BoxIn,Q.Remark,ReceivingDate,
				Op = case when Q.Operator is null then ReworkOperator
				else Q.Operator end,
				class = case when  ModuleClass is null then 'A' 
				else ModuleClass end
				from Rework_tracebility as R Full join (select * from (
				select *,ROW_NUMBER()over(partition by SN order by timestamp desc)
				 as rn from CSILabelPrintDB.dbo.QualityJudge as QI) as a where rn = 1 )
				 as Q on R.SN collate Latin1_General_CI_AS = Q.SN WHERE  ModuleStatus = 2
				and R.sn in ('{0}') and r.ReworkDate between '{1}' and '{2}'";				
				
				sql = string.Format(sql,string.Join("','",strmp),dateTimePicker1.Value.ToString(),dateTimePicker2.Value.ToString());
			}
			
			DataTable dt = Toolsclass.PullData(sql);
			List<string> SNlist = new List<string>();
			if(dt!= null && dt.Rows.Count > 0)
			foreach ( DataRow row in dt.Rows) {	
				SNlist.Add(row[0].ToString());
				dataGridView1.Rows.Insert(dataGridView1.RowCount, new object[] {false,row[0].ToString(),row[1].ToString(),row[2].ToString(),row[3].ToString(),row[4].ToString(),row[5].ToString()});
		   }
			
			if (string.IsNullOrEmpty(TB_SN.Text)) 
				return;
			string [] Leftover = new string[strmp.Length];
			int lc = 0;
			foreach (string	sn in strmp) {
				if(!SNlist.Contains(sn))
				{
					Leftover[lc] = sn;
					lc ++;
				}
			}	
			string Ltemp = string.Join(" ", Leftover).Replace(' ','\r');
			if (!string.IsNullOrEmpty(Ltemp)) {
				DialogResult dialogResult = MessageBox.Show("Following Serial numbers are not in above list, Class will not be changed. \n"+string.Join(" ", Leftover).Replace(' ','\r') + "\nClick ok to copy serial number into Clipboard","Warning",MessageBoxButtons.OKCancel,MessageBoxIcon.Warning);
			if(dialogResult == DialogResult.OK)
				Clipboard.SetText(Ltemp);
			
			}
			
			
		}
		
		
		
		string[] GetRidOfNull(string[] sDataSet)
		{
			int count = 0;
			foreach (string SN in sDataSet) {
				if (string.IsNullOrEmpty(SN)) {
					count ++;
				}
			}
			string[] ret = new string[sDataSet.Length - count];
			count = 0;
			for (int i = 0; i < sDataSet.Length; i++) {
				if(!string.IsNullOrEmpty(sDataSet[i]))
				{
					ret[count] = sDataSet[i];
					count ++;
				}
			}
			
			return ret;
		}
		void BtselectClick(object sender, EventArgs e)
		{
			if(dataGridView1.Rows.Count > 0)
			{
				foreach (DataGridViewRow row in dataGridView1.Rows) {
				DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
						if( (bool)chk.Value == true)
							chk.Value = false;
						else 
							chk.Value = true;
				}
			}
		}
		void Bt_ApproveClick(object sender, EventArgs e)
		{
			bool IsSuc = true;
			bool res = true;
			if(CbGrade.SelectedIndex < 0)
				return;
			
			
			
			if(dataGridView1.Rows.Count > 0)
			{
				for(int i = 0; i < dataGridView1.Rows.Count ; i ++)
				{
					DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dataGridView1.Rows[i].Cells[0];
					if((bool)chk.Value == true)
					{
						if (isPacked(dataGridView1.Rows[i].Cells[1].Value.ToString()))
			            {
			            	MessageBox.Show("Module "+dataGridView1.Rows[i].Cells[1].Value.ToString()+" has already been packed.");
			            	continue;
			            }
						Approved(dataGridView1.Rows[i].Cells[1].Value.ToString(),out IsSuc);
						if (!IsSuc) 
							res = false;						
					}
				}
			}
			
			if(res)
				MessageBox.Show("Approved Successfully");
			TB_SN.Clear();
			U_HistoryClick(sender,e);
		}
		void Approved(string SN,out bool IsSuc)
		{
			TbQA.Text = TbQA.Text.Replace("\'","\"");
			IsSuc = true;
			//update rework tracebility
			string sql = @"update [Rework_tracebility] set [ModuleStatus] = 4, QAOperator = '{0}', QAComments = '{1}' where sn = '{2}'";
			sql = string.Format(sql,FormLogin.usernm,TbQA.Text.ToString(),SN);
			Toolsclass.PutsData(sql);
			string remark = "";
			remark = CbROperator.Text.ToString() +" "+TbROperator.Text.ToString()+"." +""+CbQOperator.Text.ToString()+ " "+ TbQOperator.Text.ToString();
			
			
			//update quality inspection
			string Grade = CbGrade.Text.ToString();
			sql = @"INSERT INTO  QualityJudge (SN, ModuleClass, timestamp, operator, defectcode, DefectCategory ,remark) VALUES ('"+SN+"', '"+Grade+"', getdate(), '"+FormLogin.usernm+"', '', '"+ Cb_Defect.Text.ToString() +"','"+remark+"')";
        	int ret = Toolsclass.PutsData(sql,Toolsclass.GetK3Connection());
			if (ret < 1 ) 
				IsSuc = false;
			
			
			//update rework ReworkPerformed
//			string rwk_by = "";
			
//			sql = @"select top 1 rework ReworkOperator from Rework_tracebility where SN = '{0}' and ModuleStatus = {1}";
//			sql = string.Format(sql,SN,MainForm.QA);
//			DataTable dt = Toolsclass.PullData(sql,Toolsclass.GetK3Connection());
//			if(dt!= null && dt.Rows.Count > 0)
//			{
//				rwk_by = dt.Rows[0][0].ToString();
//			}
//			remark = "Rwk by " + rwk_by +".Inspection by " + FormLogin.usernm;
			sql = "INSERT INTO ReworkPerformed VALUES ('"+SN+"', '"+remark+"', '"+TbQA.Text.ToString()+"', getdate())";
			ret = Toolsclass.PutsData(sql,Toolsclass.GetK3Connection());
			if (ret < 1 ) 
				IsSuc = false;
			
		}
		void BtRejectClick(object sender, EventArgs e)
		{
			if(dataGridView1.Rows.Count > 0)
			{
				for(int i = 0; i < dataGridView1.Rows.Count ; i ++)
				{
					DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)dataGridView1.Rows[i].Cells[0];
					if((bool)chk.Value == true)
					{
						Reject(dataGridView1.Rows[i].Cells[1].Value.ToString());
					}
				}
			}
			TB_SN.Clear();
			U_HistoryClick(sender,e);
			
		}
		void Reject(string SN)
		{
			string sql = @"update [Rework_tracebility] set [ModuleStatus] = 5, QAOperator = '{0}', QAComments = '{1}' where sn = '{2}'";
			sql = string.Format(sql,FormLogin.usernm,TbQA.Text.ToString(),SN);
			Toolsclass.PutsData(sql);
		}
		void Checkbox_dateCheckedChanged(object sender, EventArgs e)
		{
			if(checkbox_date.Checked == false)
				{
					dateTimePicker1.Enabled = false;
					dateTimePicker2.Enabled = false;
					
				}
				else
				{
					dateTimePicker1.Enabled = true;
					dateTimePicker2.Enabled = true;
				}
		}
		void BtClearClick(object sender, EventArgs e)
		{
			dataGridView1.Rows.Clear();
			CbGrade.SelectedIndex = -1;
			Cb_Defect.SelectedIndex = -1;
			TB_SN.Clear();
			TbQA.Clear();
			
		}
		
		private bool isPacked(string sSerialNumber)
        {
            string sql = "select BoxID from Product where SN='{0}' and len(boxid)>0";
            sql = string.Format(sql,sSerialNumber);
            DataTable dt = Toolsclass.PullData(sql);
            if(dt != null && dt.Rows.Count > 0)
            {
            	return true;
            }
            return false;
        }
		
	}
}
