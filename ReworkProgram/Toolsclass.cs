﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 1:20 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Windows.Forms;
using System.Net.Mail;

namespace ReworkProgram
{
	/// <summary>
	/// Description of Toolsclass.
	/// </summary>
	public partial class Toolsclass : Form
	{
		public Toolsclass()
		{
		
		}
		
		public static string GetConnection()
		{
			string DataSource = getConfig ("DataSource",false,"","config.xml");	
			string UserID = getConfig ("UserID",false,"","config.xml");	
			string Password = getConfig ("Password",false,"","config.xml");	
			string DBName = getConfig ("DBName",false,"","config.xml");	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}
		public static string GetK3Connection()
		{
			string DataSource = getConfig ("DataSource",false,"","config.xml");	
			string UserID = getConfig ("UserID",false,"","config.xml");	
			string Password = getConfig ("Password",false,"","config.xml");	
			string K3DBName = getConfig ("K3DBName",false,"","config.xml");	
			string ConCsiDB = "Data Source="+DataSource+";Initial Catalog="+K3DBName+";User ID="+UserID+";Password="+Password;
			return ConCsiDB;
		}

		 public static SqlDataReader GetDataReader(string SqlStr,string sDBConn="")
        {
            SqlDataReader dr = null;
            string ConnStr = "";
            if(string.IsNullOrEmpty(sDBConn))
            	ConnStr =  GetConnection();
            else
            ConnStr = sDBConn;
         
            System.Data.SqlClient.SqlConnection conn = new System.Data.SqlClient.SqlConnection(ConnStr);
            using (System.Data.SqlClient.SqlCommand cmd = conn.CreateCommand())
            {
                cmd.CommandText = SqlStr;
                cmd.CommandType = CommandType.Text;
                try
                {
                    if (conn.State == ConnectionState.Closed) 
                        conn.Open();
                     dr = cmd.ExecuteReader();//System.Data.CommandBehavior.SingleResult
                   
                }
                catch (Exception ex)
                {
                	
                	WriteErrorLog("Exception Message",SqlStr,ex.Message);
                    dr = null;
                }
                finally
	            {
	             
	            }
               
            }
           
            return dr;
        }

		 public static DataTable PullData(string query,string connString = "")
    	{
		 	if(string.IsNullOrEmpty(connString))
		 		connString = GetConnection();
		 	
		 	try{
		    		DataTable dt = new DataTable();
        			SqlConnection conn = new SqlConnection(connString);        
      				SqlCommand cmd = new SqlCommand(query, conn);
       				conn.Open();

       				 // create data adapter
        			SqlDataAdapter da = new SqlDataAdapter(cmd);
        			// this will query your database and return the result to your datatable
       			 	da.Fill(dt);
        			conn.Close();
        			da.Dispose();
        			return dt;
		    	}
		    	catch (Exception e)
		    	{
		    		WriteErrorLog(e.Message,"","");
		    		return null;
		    	}
		    
		
    	}
		 public static int PutsData(string query,string connString = "")
   		{
		    if(string.IsNullOrEmpty(connString))
		 		connString = GetConnection();		
		 	try{
		    		SqlConnection conn = new SqlConnection(connString);
		    		conn.Open();
   					SqlCommand myCommand = new SqlCommand(query,conn);

   					int i = myCommand.ExecuteNonQuery();   					
   					conn.Close();
   					return i;
		    	}
		    	catch (Exception ex)
		    	{
		    		WriteErrorLog("Exception Message",query,ex.Message);
		    		return 0;								    	
		    	}
    	}
		 public static string getConfig(string sNodeName, bool isAttribute = false, string sAttributeName = "", string configFile = "")//(int CartonQty, int IdealPower, int MixCount, int PrintMode, string ModuleLabelPath, string CartonLabelPath, string CartonLabelParameter, string CheckRule)
        {
            string svalue = "";
            if (configFile == "" || configFile == null)
                configFile = "config.xml";
            string xmlFile = AppDomain.CurrentDomain.BaseDirectory + "\\" + configFile;
            if (!System.IO.File.Exists(xmlFile))
            {    
            	return "";
            }
            try
            {
                XmlReader reader = XmlReader.Create(xmlFile);
                if (reader.ReadToFollowing(sNodeName))
                {
                    if (!isAttribute)
                        svalue = reader.ReadElementContentAsString();
                    else
                        svalue = reader.GetAttribute(sAttributeName);
                }
                reader.Close();
            }
            catch (Exception ex)
            {
              	System.Windows.Forms.MessageBox.Show(ex.Message, "Exception Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            	return "";
            }
            if (svalue != null)
                svalue = svalue.Replace('\r', ' ').Replace('\n', ' ').Trim();
            else
                svalue = "";
            return svalue;
        }

		  public static void WritetoFile(string C,string template, string FilePath = "")
		  {
		  	if(FilePath == "")
		  		FilePath = Application.StartupPath + "\\Log.txt";
		  	
		  	File.AppendAllText(FilePath, Environment.NewLine + C + "	 " + template + "	 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
		  }
		  
		  public static void WriteErrorLog(string C,string template,string error)
		  {
//		  	string FilePath = Application.StartupPath + "\\ErrorLog.txt";
//		  	File.AppendAllText(FilePath, Environment.NewLine + C + "	 " + template + "	 " + error+ "	 " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));		  
		  		StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\ErrorLog.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + C + "	" + template + "	" + error);
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
		  
		  }
		  
	        public static void WriteToLog(string C,string template)
	        {
	            StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory +"\\Log.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + Environment.NewLine + C + "	" + template);
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
	        }
	        
	        public static void WriteToLogList(string C,string[] template)
	        {
	            StreamWriter sw = null;
	            try
	            {
	                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory +"\\Log.txt", true);
	                sw.WriteLine(GetTimestamp(DateTime.Now) + ": " + Environment.NewLine + C );
	                foreach (string  sn in template) {
	                	sw.WriteLine(Environment.NewLine + sn);
	                }
	                sw.Flush();
	                sw.Close();
	            }
	            catch
	            {
	            }
	        }
		  
		public static bool Isadmin(string user)
		{
			string sql = @"select usergroup from userlogin where usernm = '"+user+"' and CreateUser = 9"; // check for authorization for dropdown.
			DataTable dt = PullData(sql);
			if (dt!= null && dt.Rows.Count > 0) 
				return true;
			return false;
		}
		  
		   
		public static void SendEmail(string body)
        {
           	
        	try
            {
        		string host = getConfig("smtphost",false,"","Config.xml");
        		string port =  getConfig("smtpport",false,"","Config.xml");
        		string MailFrom = getConfig("EmailFrom",false,"","Config.xml");
        		string MailTo = getConfig("EmailTo",false,"","Config.xml");
        		string subject = getConfig("Subject",false,"","Config.xml");
        		string userNM = getConfig("userNM",false,"","Config.xml");
        		string PsW = getConfig("PassWord",false,"","Config.xml");
        		string[] Receipts = MailTo.Split(';');
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient(host);

                mail.From = new MailAddress(MailFrom);
                foreach (string r in Receipts)
                	mail.To.Add(r);
                mail.Subject = subject;
                mail.Body = body;
                SmtpServer.Port = Convert.ToInt16(port);   
               	SmtpServer.Credentials = new System.Net.NetworkCredential(userNM, PsW);          
                SmtpServer.EnableSsl = true;
                SmtpServer.Send(mail);
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }     
		public static String GetTimestamp(DateTime value)
        {
            return value.ToString("dd'/'MM'/'yy HH:mm:ss:ffff");
        }
	}
}
