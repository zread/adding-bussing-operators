﻿/*
 * Created by SharpDevelop.
 * User: jacky.li
 * Date: 6/21/2016
 * Time: 10:22 AM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace ReworkProgram
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public const int RS = 0; //Module received
		public const int RF = 1; //Delivered to other department
		public const int QA = 2; //Quailty check required	
		public const int Eng = 3; // Engineering support required
		public const int AQ = 4; // QA done
		public const int RQ = 5; // QA Reject
		public const int ED = 6; // ENGINEERING FINISH
		public const int RU = 7; // Others
		public const int DONE = 8; // finished
		public const int Scrap = 9; //scrap
		
		// location
		public const int PL = 0; //Production line 
		public const int LB = 1; //LAB
		public const int MR = 2; //MRB
		public const int WH = 3; //Warehouse
		public const int OT = 4; //Other
		public const int RK = 5; //Rework
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			TsslUser.Text = FormLogin.usernm;
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
		}
		void BtNewClick(object sender, EventArgs e)
		{
			if(FormLogin.userID != 9 && !Toolsclass.Isadmin(FormLogin.usernm) && FormLogin.userID != 10)
				return;
			FormAddNew.Instance(this);
		}
			
	
	}
}
